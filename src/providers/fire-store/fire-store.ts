import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {AngularFirestore, AngularFirestoreCollection} from "angularfire2/firestore";
import {AngularFireAuth} from "angularfire2/auth";
import {FirebaseLoginProvider} from '../firebase-login/firebase-login';
import {Http} from "@angular/http";
import firebase from 'firebase';
import {FileTransfer, FileTransferObject,} from '@ionic-native/file-transfer';
import {File} from '@ionic-native/file';

declare var cordova: any;

interface Users {
    uid: string;
    displayName?: string;
    email: string;
    location: any;
    photoURL: string;
    fbPhoto: string;
    lastSeen: string;
}

@Injectable()
export class FireStoreProvider {

    public users: any;
    public auth: any;
    items: Observable<Users[]>;
    itemsCollection: AngularFirestoreCollection<Users>;
    public link: string;
    user: Observable<Users>;
    us: Users;
    storageDirectory: string = '';
    a: string;
    storageRef: any;
    photo: any;
    fileTransfer: FileTransferObject = this.transfer.create();

    fi: File;

    constructor(public db: AngularFirestore,
                public fb: FirebaseLoginProvider,
                public afAuth: AngularFireAuth,
                private http: Http,
                private transfer: FileTransfer,
                private file: File,
                private firebaseLoginProvider: FirebaseLoginProvider) {

        // this.itemsCollection = this.db.collection('users', ref => {
        //     return ref.where('');
        // });
        this.itemsCollection = this.db.collection('development/yuJ5CCJDP5yFDRWgMqQC/users');

        this.storageDirectory = 'file:///data/user/0/com.forbored.app/files/'; //cordova.file.dataDirectory; //DISABLED, ERROR: Cannot read property 'dataDirectory' of undefined
    }

    getAuth() {
        return this.afAuth;
    }

    getUID() {
        return this.afAuth.auth.currentUser.uid;
    }

    downloadImage(image) {
        const fileTransfer: FileTransferObject = this.transfer.create();
        const filename2 = this.afAuth.auth.currentUser.uid;
        var dir = cordova.file.externalDataDirectory + 'Download/';
        const filename = Math.floor(Date.now() / 1000);
        fileTransfer.download(image, dir + filename + ".jpg").then((entry) => {
            console.log(entry.toURL() + "NAUJAS");
            this.uploadFile(entry, filename2);
        });
    }

    uploadFile(fileEntry, filename) {
        fileEntry.file(function (resFile) {

            // const filename= this.afAuth.auth.currentUser.uid;
            //let filename = Math.floor(Date.now() / 1000);
            var storageRef = firebase.storage().ref();
            const imageRef = storageRef.child('images/' + filename + '.jpg');

            var reader = new FileReader();
            reader.onloadend = function (evt) {

                var imgBlob = new Blob([this.result], {type: 'image/jpeg'});

                imageRef.put(imgBlob);

            };
            reader.readAsArrayBuffer(resFile);

        });
    }

    getUsers() {
        this.items = this.itemsCollection.valueChanges();
        return this.items;
    }


    updateParticants()
    {
        let storage = firebase.firestore()
            .collection('development/yuJ5CCJDP5yFDRWgMqQC/users')
            .where('fbFriends.' + this.firebaseLoginProvider.user.providerData[0].uid, '==', true)
            .limit(30);

        storage.get().then((snap)=>{
            console.log(snap, 'snap friends');
            this.users = snap.docs;
        });
    }



    setLocation(loc) {
        if(this.fb.user && this.fb.user.uid) {
            this.itemsCollection.doc(this.fb.user.uid).update(
                {
                    location: loc,
                    lastSeen: Date.parse(new Date().toISOString())
                }
            );
        } else {
            console.error('Can`t retrieve uid.');
        }
    }

    updateFirebaseWithData() {
        let sub = this.itemsCollection.doc(this.fb.user.uid).valueChanges().subscribe((url: any) => {
            //TODO generalize photo retrieval
            let data = {
                uid: this.fb.user.uid,
                photoURL: this.fb.user.providerData[0].photoURL,
                email: this.fb.user.email,
                displayName: this.fb.user.displayName,
                lastSeen: Date.parse(new Date().toISOString())
                // fbPhoto: this.fb.user.photoURL
            };

            this.itemsCollection.doc(this.fb.user.uid).update(data).catch((reject) => {
                console.log("updateFirebaseWithData failed");
            });

            console.log("USER INFO UPDATED");

            sub.unsubscribe();
        });
    }
}
