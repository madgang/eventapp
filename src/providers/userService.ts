import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
    constructor() {
        this.avatarPromise = new Promise((resolve) => {
            this.isAvatarLoaded = resolve;
        });
    }
    userId: any;
    isAvatarLoaded: any;
    avatarPromise: any;

    setUserAvatar (avatar: string) {
        this.isAvatarLoaded = avatar;
    }

    setUserId (id:string)
    {
        this.userId=id;
    }
    getUserId ()
    {
        return this.userId;
    }
    getUserAvatar () {
        return this.avatarPromise;
    }

    getUserAvatarRaw () {
        return this.isAvatarLoaded || "";
    }
}
