import { Injectable } from '@angular/core';

import firebase from 'firebase/app';
import 'firebase/storage';

import { storageEnvironment } from '../../app/environment';

@Injectable()
export class FirebaseStorageProvider {

  constructor() { }

  public putEvent(id: string, data: any): firebase.storage.UploadTask {
    return firebase.storage().ref().child(storageEnvironment + '/events/' + id).put(
      data, 
      {
        contentType: 'image/jpeg'
      }
    );
  }

}
