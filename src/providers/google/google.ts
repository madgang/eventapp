import { Injectable } from '@angular/core';

import { GooglePlus } from '@ionic-native/google-plus';

import { webClientId } from '../../app/credentials';

@Injectable()
export class GoogleProvider {

  silentLoginError: any;
  silentLoginResponse: any;

  constructor(
    private googlePlus: GooglePlus
  ) { }

  login(): Promise<any> {
    return this.googlePlus.login({ //EDIT
      'webClientId': webClientId
    });
  }

  trySilentLogin(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.googlePlus.trySilentLogin({ //EDIT
        'webClientId': webClientId
      })
        .then((response: any) => {
          this.silentLoginResponse = response;
          resolve(response);
        })
        .catch((error: any) => {
          this.silentLoginError = error;
          reject(error);
        });
    });
  }

}
