import {Injectable} from '@angular/core';

import {Facebook, FacebookLoginResponse} from '@ionic-native/facebook';

@Injectable()
export class FacebookProvider {
    roles: any = ['public_profile', 'user_friends', 'email', 'user_events'];

    //loginStatusError: any; //TODO: enable with getLoginStatus()
    //loginStatusResponse: any; //TODO: enable with getLoginStatus()

    constructor(
        private facebook: Facebook
    ) {
    }

    getAccessToken(): Promise<string> {
        return this.facebook.getAccessToken();
    }

    /* getLoginStatus(): Promise<any> { //INFO: not working if it is called before login()
      return new Promise((resolve, reject) => {
        this.facebook.getLoginStatus()
          .then((response: any) => {
            this.loginStatusResponse = response;
            resolve(this.loginStatusResponse);
          })
          .catch((error: any) => {
            this.loginStatusError = error;
            reject(this.loginStatusError);
          });
      });
    } */

    login(): Promise<FacebookLoginResponse> {
        return this.facebook.login([ //EDIT, INFO: https://developers.facebook.com/docs/facebook-login/permissions
            'public_profile',
            'email'
        ]);
    }

    getMyFriends() {
        return this.facebook.api(`/me/friends`, this.roles);
    }

}
