import {Http} from '@angular/http';
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Facebook} from "@ionic-native/facebook";

@Injectable()
export class FbEventsService {
    roles: string[];

    constructor(private fb: Facebook, private http: Http) {
        this.roles = ['public_profile', 'user_friends', 'email', 'user_events'];
    }

    static test() {
        console.log("test");
    }

    getUserToken() {
        return this.fb.getAccessToken();
    }

    getEvents(url) {
        return this.http.get(url).map(res => res.json());
    }

    getEventByQuery(query) {
        return this.fb.api('/search?type=event&q=vilnius&limit=5', this.roles);
    }

    getEventById(eventId) {
        return this.fb.api(`/${eventId}?fields=description,cover`, this.roles);
    }

    getAdditionalEventData(eventId, fieldsString) {
        return this.fb.api(`/${eventId}/?fields=${fieldsString}`, this.roles);
    }

    getFbService() {
        return this.fb;
    }

    setParsedEventList(eventsList, infiniteScroll, scrollMore) {
        return this.getEvents(scrollMore);
    }

    getMyFriends(){
        return this.fb.api(`/me/friends`, this.roles);
    }
}

