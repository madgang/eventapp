import { Injectable } from '@angular/core';
import {
  isNumber,
  isString
} from 'ionic-angular/util/util';

import { FirebaseFirestoreProvider } from '../firebase-firestore/firebase-firestore';

interface Error {
  code: string;
  message: string;
}

@Injectable()
export class ErrorsProvider {

  constructor(
    private firebaseFirestoreProvider: FirebaseFirestoreProvider
  ) { }

  setError(error: any, info: string): void {
    let checkedError = this.checkError(error);

    this.firebaseFirestoreProvider.set(
      '/errors/' + new Date(),
      {
        code: checkedError.code,
        info: info,
        message: checkedError.message
      }
    )
      .catch(() => { });
  }

  setUserError(error: any, info: string): void {
    let checkedError = this.checkError(error);

    this.firebaseFirestoreProvider.setUser(
      '/errors/' + new Date(),
      {
        code: checkedError.code,
        info: info,
        message: checkedError.message
      }
    )
      .catch(() => { });
  }

  private checkError(error: any): Error {
    let checkedError = { } as Error;

    if (isString(error.code)) {
      checkedError.code = error.code;
    } else if (isNumber(error.code)) {
      checkedError.code = error.code.toString();
    } else {
      checkedError.code = null;
    }

    if (isString(error.message)) {
      checkedError.message = error.message;
    } else if (isNumber(error.message)) {
      checkedError.message = error.code.toString();
    } else if (isString(error)) {
      checkedError.message = error;
    } else if (isNumber(error)) {
      checkedError.message = error.toString();
    } else {
      checkedError.message = null;
    }

    return checkedError;
  }

}
