import { Injectable, Query } from '@angular/core';

import firebase from 'firebase/app';
import 'firebase/firestore';

import { firestoreEnvironment } from '../../app/environment';

@Injectable()
export class FirebaseFirestoreProvider {

    constructor() { }

    public add(collectionPath: string, data: firebase.firestore.DocumentData): Promise<firebase.firestore.DocumentReference> {
        return firebase.firestore().collection(firestoreEnvironment + collectionPath).add(data);
    }

    public addUser(collectionPath: string, data: firebase.firestore.DocumentData): Promise<firebase.firestore.DocumentReference> {
        return firebase.firestore().collection(firestoreEnvironment + '/users/' + this.currentUser().uid + collectionPath).add(data);
    }

    public currentUser(): firebase.User {
        return firebase.auth().currentUser;
    }

    public getCollection(collectionPath: string): Promise<firebase.firestore.QuerySnapshot> {
        return firebase.firestore().collection(firestoreEnvironment + collectionPath).get();
    }

    public getDoc(documentPath: string): Promise<firebase.firestore.DocumentSnapshot> {
        return firebase.firestore().doc(firestoreEnvironment + documentPath).get();
    }

    public getUserCollectionGoingEvents(): Promise<firebase.firestore.QuerySnapshot> {
        return firebase.firestore().collection(firestoreEnvironment + '/users/' + this.currentUser().uid + '/goingEvents').orderBy('startTime').get();
    }

    public getUserDoc(documentPath: string): Promise<firebase.firestore.DocumentSnapshot> {
        return firebase.firestore().doc(firestoreEnvironment + '/users/' + this.currentUser().uid + documentPath).get();
    }

    public set(documentPath: string, data: firebase.firestore.DocumentData): Promise<void> {
        return firebase.firestore().doc(firestoreEnvironment + documentPath).set(data, {merge: true});
    }

    public setUser(documentPath: string, data: firebase.firestore.DocumentData): Promise<void> {
        return firebase.firestore().doc(firestoreEnvironment + '/users/' + this.currentUser().uid + documentPath).set(data, {merge: true});
    }

    addCollectionCurrentUser (collectionPath: string, data: firebase.firestore.DocumentData): Promise<firebase.firestore.DocumentReference>{
        return firebase.firestore().collection(firestoreEnvironment + '/users/' + this.currentUser().uid + collectionPath ).add(data);
    }

    setUserData(user: any, language: any){
        this.setUser('', {
            settings: {
                language: language
            },
            displayName: user.displayName,
            email: user.email,
            metadata: {
                creationTime: user.metadata.creationTime,
                lastSignInTime: user.metadata.lastSignInTime
            },
            phoneNumber: user.phoneNumber,
            photoURL: user.providerData[0].photoURL, //TODO: check if need all array elements
            fbUid: user.providerData[0].uid,
            lastSeen: Date.parse(new Date().toISOString()),
            uid: user.uid
        }).catch((error: any) => {
            console.error('Failed to update user data');
        });
    }

    setUserCustomData(options: any){
        this.setUser(options.path || '', options.data).catch((error: any) => {
            console.error('Failed to update user data');
        });
    }

}
