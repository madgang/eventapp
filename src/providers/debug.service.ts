import { Injectable } from '@angular/core';

@Injectable()
export class DebugService {
    private _isDebug = true;
    public isBrowser: any  = false;
    constructor() {

    }

    get isDebug(): boolean {
        return this._isDebug;
    }

    set isDebug(value: boolean) {
        this._isDebug = value;
    }

    public getDebugMapConfig(){
        return {
            desiredAccuracy: 0,
            stationaryRadius: 0,
            distanceFilter: 0,
            debug: false, //  enable this hear sounds for background-geolocation life-cycle.
            stopOnTerminate: false, // enable this to clear background location settings when the app terminates
            fastestInterval: 3000,
            interval: 3000,
            stopOnStillActivity: false,
            activitiesInterval: 0,
            locationProvider: 1,
            saveBatteryOnBackground: true,
            maxLocations: 0,
            startForeground: false
        }
    }
}
