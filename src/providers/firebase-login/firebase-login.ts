import { Injectable } from '@angular/core';

import firebase from 'firebase/app';
import 'firebase/auth';

@Injectable()
export class FirebaseLoginProvider {

  user: firebase.User;
  signInError: any;

  constructor() { }

  private signInWithCredential(credential: firebase.auth.AuthCredential): Promise<firebase.User> {
    return new Promise((resolve, reject) => {
      firebase.auth().signInWithCredential(credential)
        .then((user: firebase.User) => {
          this.user = user;
          resolve(user);
        })
        .catch((error: any) => {
          this.signInError = error;
          reject(error);
        });
    });
  }

  signInWithFacebook(token: string): Promise<firebase.User> {
    let credential = firebase.auth.FacebookAuthProvider.credential(token);
    return this.signInWithCredential(credential);
  }

  signInWithGoogle(idToken: string): Promise<firebase.User> {
    let credential = firebase.auth.GoogleAuthProvider.credential(idToken);
    return this.signInWithCredential(credential);
  }

}
