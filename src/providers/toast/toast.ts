import { Injectable } from '@angular/core';

import {
  Toast,
  ToastController
} from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class ToastProvider {

  private errorToast: Toast;

  constructor(
    private translateService: TranslateService,
    private toastCtrl: ToastController
  ) { }

  presentInfoToast(key: string): Promise<any> {
    let message = this.translateHelper(key);
 
    let toast = this.toastCtrl.create({
      message: message,
      duration: 1500, //EDIT
      position: "top",
      dismissOnPageChange: true
    });

    return toast.present();
  }

  presentErrorToast(key: string): Promise<any> {
    let translation = this.translateHelper(['ERROR', key, 'TOAST_CLOSE_BUTTON']);

    if (this.errorToast) {
      this.errorToast.dismiss();
    }
        
    this.errorToast = this.toastCtrl.create({
      message: translation['ERROR'] + translation[key],
      showCloseButton: true,
      closeButtonText: translation['TOAST_CLOSE_BUTTON'],
      dismissOnPageChange: true
    });

    return this.errorToast.present();
  }

  private translateHelper(key: string | string[]): any {
    let translation: any;

    this.translateService.get(key)
      .subscribe((value: any) => {
        translation = value;
      });

    return translation;
  }

}
