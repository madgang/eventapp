import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from "angularfire2/firestore";
import {FirebaseLoginProvider} from "./firebase-login/firebase-login";
import {FirebaseFirestoreProvider} from "./firebase-firestore/firebase-firestore";
import * as firebase from "firebase";

@Injectable()
export class FireEventService {
    public friendCollection: any;
    public limit: number;
    public lastSnap: any;
    public max: number;
    private START_TIME = "event.time.startTime";

    constructor(public db: AngularFirestore, public firebaseLoginProvider: FirebaseLoginProvider,  private firebaseFirestoreProvider: FirebaseFirestoreProvider,) {
        //this.itemsCollection.limit(10);
    }

    /**
     * Retrieve data like in example
     * snapshot.docs[0].data() === {event data}
     * @returns {any}
     */
    getAllFriendEvents(limit: number) {
        this.limit = limit;
        this.friendCollection = this.db.firestore.collection('development/yuJ5CCJDP5yFDRWgMqQC/events')
            .orderBy(this.START_TIME)
            .where(this.START_TIME, '>', new Date().getTime())
            .limit(limit);

        this.enableLoadingMoreIfPossible();

        return this.friendCollection.get();
    }


    getUid(){
        return this.firebaseLoginProvider.user.providerData[0].uid;
    }

    addParticipant(eventId) {
        this.firebaseFirestoreProvider.add('/events/'+eventId+'/participants',
        {
            participant:{
                id: this.firebaseLoginProvider.user.providerData[0].uid, name: this.firebaseLoginProvider.user.providerData[0].displayName
        }
    });
    }

    getParticipantsLenght(docReference: any) {
        this.db.firestore.collection(docReference.ref.path + '/participants').get().then((snap) => {
            docReference.participantsCount = snap.docs.length;
        });
    }

    getParticipants(docReference: any) {
        this.db.firestore.collection(docReference.ref.path + '/participants').get().then((snap) => {
            docReference.participants = snap.docs;
            console.log(snap.docs+"ciaaa");
        });
    }

    enableLoadingMoreIfPossible(){
        this.friendCollection.get().then((snap)=>{
            if (snap.docs.length > 0 && snap.docs.length == this.limit){
                this.lastSnap = snap.docs[this.limit -1];
            } else {
                this.lastSnap = false;
            }
        });
    }

    loadMoreEvents() {
        if (this.lastSnap){
            this.friendCollection = this.db.firestore.collection('development/yuJ5CCJDP5yFDRWgMqQC/events')
                .orderBy(this.START_TIME)
                .where(this.START_TIME, '>', new Date().getTime())
                .startAfter(this.lastSnap)
                .limit(this.limit);

            this.enableLoadingMoreIfPossible();

            return this.friendCollection.get();
        }
        return false;
    }

    getAllEventSubscriber(eventsLimit: number) {
        return this.db.collection('development/yuJ5CCJDP5yFDRWgMqQC/events', ref => ref.limit(eventsLimit)).valueChanges();
    }
}
