import {Injectable} from '@angular/core';

@Injectable()
export class EventMarkerService {
    constructor() {
    }

    public currentEvent: { event: any, location: any };
}

/*{
    "eid": "3oIKit2P3bniUkiMmWCN",
    "event": {
        "creationDate": "2018-05-03T15:21:52.875Z",
        "description": "pasipisam",
        "img": "",
        "time": {"endTime": null, "startTime": 1525533660000},
        "title": "testas3000"
    },
    "location": {"lat": 54.7141848, "lng": 25.2732469},
    "locationQuery": "Ozas, Ozo gatvė, Vilnius, Lithuania",
    "user": {"uid": "L2ZFyQuL91MWSZUOyy0nOjCglo43"}*/

