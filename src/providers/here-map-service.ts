import {Injectable} from '@angular/core';

declare var H: any;

@Injectable()
export class HereMap {
    private _here: any;

    constructor() {
        this._here = H;
        this._platform = new H.service.Platform({
            'app_id': 'cNYV5PR81FoHrAaCj7gl',
            'app_code': 'g7RFZD6vRk4Smq1Mj3Rc0w'
        });
    }

    private _platform: any;

    get platform(): any {
        return this._platform;
    }

    set platform(value: any) {
        this._platform = value;
    }

    get H(): any {
        return this._here;
    }

    set H(value: any) {
        this._here = value;
    }

    domIcon(domIconOptions: any) {
        let H = this.H;
        let options = {};
        var outerElement = document.createElement('div');

        if (domIconOptions.element){
            outerElement = domIconOptions.element;
        } else {
            if (domIconOptions.html) {
                outerElement.innerHTML = domIconOptions.html;
            }
        }

        if (domIconOptions.methodToCall) {
            options = {
                onAttach: (clonedElement, domIcon, domMarker) => {
                    clonedElement.addEventListener('touchend', domIconOptions.methodToCall);
                },
                onDetach: (clonedElement, domIcon, domMarker) => {
                    clonedElement.removeEventListener('touchend', domIconOptions.methodToCall);
                }
            }
        }
        let icon = new H.map.DomIcon(outerElement, options);
        return icon;
    }

    loadHereMap(coords, element) {
        let H = this.H;

        let defaultLayers = this.platform.createDefaultLayers(512, 320);

        let map = new this.H.Map(
            /*Check type if element pass element if not get by id*/
            element.click && element ||  document.getElementById(element),
            defaultLayers.normal.map, {
                imprint: {font: "0px Arial,sans-serif"},
                pixelRatio: 2
            });


        let behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
        let ui = H.ui.UI.createDefault(map, defaultLayers);

        map.setCenter(coords);
        map.setZoom(18);
        map.ui = ui;

        return map;
    }
}
