import {Injectable} from '@angular/core';
import {GoogleMap, ILatLng, Marker} from "@ionic-native/google-maps";


@Injectable()
export class CreateEventService {
    private _location: ILatLng;
    private _map: GoogleMap;
    private _query: String = 'Location';
    private _marker: Marker;

    public setLocationValue: any;

    constructor() {

    }

    get location(): ILatLng {
        return this._location;
    }

    set location(value: ILatLng) {
        this._location = value;
    }

    get map(): GoogleMap {
        return this._map;
    }

    set map(value: GoogleMap) {
        this._map = value;
    }

    get query(): String {
        return this._query;
    }

    set query(value: String) {
        this._query = value;
    }

    get marker(): Marker {
        return this._marker;
    }

    set marker(value: Marker) {
        this._marker = value;
    }


}
