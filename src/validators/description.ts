import {
    ValidatorFn,
    Validators
} from '@angular/forms';

export class DescriptionValidator {
    static isValid (): ValidatorFn {
        return Validators.compose([
            Validators.required,
            Validators.maxLength(150) //EDIT
        ]);
    }
}
