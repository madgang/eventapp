import {
    ValidatorFn,
    Validators
} from '@angular/forms';

export class TitleValidator {
    static isValid (): ValidatorFn {
        return Validators.compose([
            Validators.required,
            Validators.maxLength(30) //EDIT
        ]);
    }
}
