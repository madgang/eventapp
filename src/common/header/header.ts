import {AfterContentInit, Component} from '@angular/core';
import {EventPage} from '../event/event';
import {CreateEventPage} from "../create_event/create-event";
import {MenuController} from "ionic-angular";
import {UserService} from "../../providers/userService";
import * as firebase from "firebase/app";
import {FbEventsService} from "../../providers/fb-events-service";
import {AngularFireDatabase} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";

@Component({
    selector: 'header',
    templateUrl: 'header.html',
    inputs: ['title']
})

export class Header implements AfterContentInit {
    userData: UserService;
    avatar: string;

    constructor (public menuCtrl: MenuController,
                 public user: UserService,
                 public fbEvents: FbEventsService,
                 auth: AngularFireAuth,
                 afd: AngularFireDatabase,) {
        menuCtrl.enable(true);
        this.userData = user;
        this.getAvatarUrl();
        this.loginUserToFireBase(this.fbEvents, user);
    }

    ngAfterContentInit(): void {

    };

    getAvatarUrl() {
        this.userData.getUserAvatar().then((avatar: string) => {
            this.avatar = avatar;
        });
    }

    loginUserToFireBase (fbEvents, user) {
        var userToken = fbEvents.getUserToken().then((token) => {
            var credential = firebase.auth.FacebookAuthProvider.credential(token);
            firebase.auth().signInWithCredential(credential)
                .then((userData) => {

                    var userDataToSave = {
                        photoURL: userData.photoURL,
                        email: userData.email,
                        displayName: userData.displayName
                    };

                    this.avatar = userData.photoURL;
                    user.setUserAvatar(userData.photoURL);

                    var link = '/users/' + userData.uid;
                    firebase.firestore().collection('users').doc(userData.uid).set(userDataToSave);

                    console.log(userData);
                })
                .catch(function (error) {
                    console.log(error);
                });
        });
    }
}
