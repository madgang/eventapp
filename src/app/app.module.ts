import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule, IonicPageModule} from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { HttpModule } from '@angular/http';

// Translation
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

// Facebook
import { Facebook } from "@ionic-native/facebook"
import { FacebookModule } from 'ngx-facebook'; //TODO: delete

// Google
import { GooglePlus } from '@ionic-native/google-plus';

// Pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

// Providers
import { ErrorsProvider } from '../providers/errors/errors';
import { FacebookProvider } from '../providers/facebook/facebook';
import { FirebaseFirestoreProvider } from '../providers/firebase-firestore/firebase-firestore';
import { FirebaseLoginProvider } from '../providers/firebase-login/firebase-login';
import { FirebaseStorageProvider } from '../providers/firebase-storage/firebase-storage';
import { FireStoreProvider } from '../providers/fire-store/fire-store';
import { GoogleProvider } from '../providers/google/google';
import { ToastProvider } from '../providers/toast/toast';

import { EventService } from '../providers/event-service';
import { FbEventsService } from '../providers/fb-events-service';
import { UserService } from "../providers/userService";

// AngularFire
import { firebaseConfig } from './credentials';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireDatabaseModule } from 'angularfire2/database';

// Others
import { BackgroundGeolocation } from "@ionic-native/background-geolocation";
import { Diagnostic } from "@ionic-native/diagnostic";
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from '@ionic-native/google-maps';
import { IonicImageLoader } from "ionic-image-loader";
import { LocationAccuracy } from "@ionic-native/location-accuracy";
import { MapService } from "../pages/map/map.service";
import {CreateEventService} from "../providers/create-event-service";
import {HereMap} from "../providers/here-map-service";
import {DebugService} from "../providers/debug.service";
import {SingleEventPage} from "../pages/single-event/single-event";
import {SingleEventPageModule} from "../pages/single-event/single-event.module";
import {FireEventService} from "../providers/fire.event.service";
import {DomService} from "../providers/inject.service";
import {EventMarkerComponent} from "../components/event-marker/event-marker";
import {ModalPage} from "../pages/map/clustering.helper";
import {EventMarkerService} from "../providers/eventMarkerService";
import {TabService} from "../providers/tab.service";

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        EventMarkerComponent,
        ModalPage
    ],
    imports: [
        IonicPageModule.forChild(ModalPage),
        BrowserModule,
        IonicModule.forRoot(MyApp),
        HttpModule,
        InMemoryWebApiModule.forRoot(InMemoryDataService, {passThruUnknownUrl: true}),
        FacebookModule.forRoot(),
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFirestoreModule,
        AngularFireAuthModule,
        AngularFirestoreModule.enablePersistence(),
        SingleEventPageModule,
        IonicImageLoader.forRoot()
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        SingleEventPage,
        EventMarkerComponent,
        ModalPage
    ],
    providers: [
        TabService,
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        AngularFireDatabase,
        AngularFireDatabaseModule,
        BackgroundGeolocation,
        Diagnostic,
        Facebook,
        Geolocation,
        GoogleMaps,
        GooglePlus,
        LocationAccuracy,
        ErrorsProvider,
        FacebookProvider,
        MapService,
        FireStoreProvider,
        FirebaseFirestoreProvider,
        FirebaseLoginProvider,
        FirebaseStorageProvider,
        GoogleProvider,
        ToastProvider,
        EventService,
        FbEventsService,
        UserService,
        CreateEventService,
        HereMap,
        DebugService,
        FireEventService,
        DomService,
        EventMarkerService
    ]
})
export class AppModule {
}

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
