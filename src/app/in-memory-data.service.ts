import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    let events = [
      {
        id: '1',
        cover: {
          source: 'http://maison-cresci.fr/uploads/images/galerie_hotel_regence_nice_slide005.jpg'
        },
        description: 'Description 1',
        name: 'Name 1'
      },
      {
        id: '2',
        cover: {
          source: 'http://maison-cresci.fr/uploads/images/galerie_hotel_regence_nice_slide005.jpg'
        },
        description: 'Description 2',
        name: 'Name 2'
      },
      {
        id: '3',
        cover: {
          source: 'http://maison-cresci.fr/uploads/images/galerie_hotel_regence_nice_slide005.jpg'
        },
        description: 'Description 3',
        name: 'Name 3'
      },
      {
        id: '4',
        cover: {
          source: 'http://maison-cresci.fr/uploads/images/galerie_hotel_regence_nice_slide005.jpg'
        },
        description: 'Description 4',
        name: 'Name 4'
      },
      {
        id: '5',
        cover: {
          source: 'http://maison-cresci.fr/uploads/images/galerie_hotel_regence_nice_slide005.jpg'
        },
        description: 'Description 5',
        name: 'Name 5'
      },
      {
        id: '6',
        cover: {
          source: 'http://maison-cresci.fr/uploads/images/galerie_hotel_regence_nice_slide005.jpg'
        },
        description: 'Description 6',
        name: 'Name 6'
      },
    ];
    return {events};
  }
}
