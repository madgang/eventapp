import {
    Component,
    ViewChild
} from '@angular/core';
import {
    Nav,
    Platform
} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import firebase from 'firebase/app';
import {TranslateService} from '@ngx-translate/core';

import {ErrorsProvider} from '../providers/errors/errors';
import {FacebookProvider} from '../providers/facebook/facebook';
//import { firebaseConfig } from './credentials'; //EDIT: enable when we will stop using AngularFire
import {FirebaseFirestoreProvider} from '../providers/firebase-firestore/firebase-firestore'; //INFO: implementation 1
import {FirebaseLoginProvider} from '../providers/firebase-login/firebase-login';
import {FireStoreProvider} from '../providers/fire-store/fire-store'; //INFO: implementation 2
import {GoogleProvider} from '../providers/google/google';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav; //TODO: check if implemented correctly

    private defaultLang: string = 'en' //EDIT: default language
    private pages: Array<{ title: string, component: any }>;
    private rootPage: any;

    constructor(
        private errorsProvider: ErrorsProvider,
        facebookProvider: FacebookProvider,
        private firebaseLoginProvider: FirebaseLoginProvider,
        private firebaseFirestoreProvider: FirebaseFirestoreProvider,
        private fireStoreProvider: FireStoreProvider,
        googleProvider: GoogleProvider,
        platform: Platform,
        splashScreen: SplashScreen,
        statusBar: StatusBar,
        private translateService: TranslateService
    ) {

        translateService.setDefaultLang(this.defaultLang);

        //firebase.initializeApp(firebaseConfig); //EDIT: enable when we will stop using AngularFire

        firebase.auth().onAuthStateChanged((user: firebase.User) => {
            if (user) {
                switch (user.providerData[0].providerId) {
                    case 'facebook.com': {
                        facebookProvider.getAccessToken()
                            .then((accessToken: string) => {
                                firebaseLoginProvider.signInWithFacebook(accessToken)
                                    .then((user: firebase.User) => {
                                        this.success(user);
                                    })
                                    .catch((error: any) => {
                                        //TODO: add proper error handling
                                        errorsProvider.setUserError(error, 'MyApp.onAuthStateChanged().getAccessToken().signInWithFacebook()'); //EDIT: error info
                                    });
                            })
                            .catch((error: any) => {
                                //TODO: add proper error handling
                                errorsProvider.setUserError(error, 'MyApp.onAuthStateChanged().getAccessToken()'); //EDIT: error info
                            });
                        break;
                    }
                    case 'google.com': {
                        googleProvider.trySilentLogin()
                            .then((response: any) => {
                                firebaseLoginProvider.signInWithGoogle(response.idToken)
                                    .then((user: firebase.User) => {
                                        this.success(user);
                                    })
                                    .catch((error: any) => {
                                        //TODO: add proper error handling
                                        errorsProvider.setUserError(error, 'MyApp.onAuthStateChanged().trySilentLogin().signInWithGoogle()'); //EDIT: error info
                                    });
                            })
                            .catch((error: any) => {
                                //TODO: add proper error handling
                                errorsProvider.setUserError(error, 'MyApp.onAuthStateChanged().trySilentLogin()'); //EDIT: error info
                            });
                        break;
                    }
                    default: {
                        //TODO: add proper error handling
                        let error: firebase.auth.Error;
                        error.message = 'switch/default';
                        errorsProvider.setUserError(error, 'MyApp.onAuthStateChanged().switch()'); //EDIT: error info
                        break;
                    }
                }

                this.rootPage = 'TabsPage'; //EDIT: homepage

            } else {
                this.rootPage = 'LoginPage'; //EDIT: login page
            }
        });

        this.pages = [ //EDIT: menu pages
            {title: 'PROFILE_PAGE', component: 'ProfilePage'},
            {title: 'SETTINGS_PAGE', component: 'SettingsPage'},
            {title: 'CREATE_EVENT_PAGE', component: 'CreateEventPage'},
            {title: 'SEARCH_PAGE', component: 'SearchPage'},
            {title: 'FAVS_PAGE', component: 'FavsPage'},
            {title: 'HELP_PAGE', component: 'HelpPage'},
            {title: 'ABOUT_PAGE', component: 'AboutPage'},
        ];

        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });

    }

    private openPage(page: any): void {
        this.nav.push(page);
    }

    private success(user: firebase.User): void {
        this.firebaseFirestoreProvider.getUserDoc('')
            .then((documentSnapshot: firebase.firestore.DocumentSnapshot) => {

                let language: string;

                if (documentSnapshot.exists) {
                    language = documentSnapshot.get('settings.language');
                } else {
                    language = this.defaultLang;
                }

                this.translateService.use(language);

                //INFO: implementation 1
                this.firebaseFirestoreProvider.setUserData(user, language);

                //INFO: implementation 2
                //this.fireStoreProvider.updateFirebaseWithData(); //TODO: use promise

            })
            .catch((error: any) => {
                //TODO: add proper error handling
                this.errorsProvider.setUserError(error, 'MyApp.success().getUserDoc()'); //EDIT: error info
            });
    }

}
