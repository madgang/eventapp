export class Event {
  id: string;
  /*attending_count: number;*/
  cover: {
  	source: string;
  };
  description: string;
  /*end_time: string;*/
  /*is_canceled: boolean;*/
  name: string;
  img: string;
  /*place: {};*/
  /*start_time: string;*/
  /*ticket_uri: string;*/
}
