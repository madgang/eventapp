import {Component, ViewContainerRef} from '@angular/core';
import {HereMap} from "../../providers/here-map-service";
import {DomService} from "../../providers/inject.service";

/**
 * Generated class for the EventMapComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

declare var H: any;

@Component({
  selector: 'event-map',
  templateUrl: 'event-map.html'
})
export class EventMapComponent {

  text: string;

  constructor(private hereMap: HereMap,
              private domService: DomService,
              private currRef: ViewContainerRef) {
        console.log('yeah');
  }

    ionViewDidLoad () {
        //this.hereMap.loadHereMap(location, 'map');

        console.log('loaded ev ent-ma-');
    }

    ngOnInit() {
        let el = document.createElement('div');
        let location = {lat: 54.7127104,  lng: 25.2895082};
        let map = this.hereMap.loadHereMap(location, el);
        this.currRef.element.nativeElement.querySelector('div').appendChild(el);
        console.log('ngOnInit');
    }

    ngAfterViewInit() {
        console.log('ngAfterViewInit');
    }

}
