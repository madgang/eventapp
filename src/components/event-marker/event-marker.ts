import {Component} from '@angular/core';
import {EventMarkerService} from "../../providers/eventMarkerService";

@Component({
    selector: 'event-marker',
    templateUrl: 'event-marker.html'
})
export class EventMarkerComponent {
    eData: any;
    multipleEvents: any;
    multiEvents: any;
    key: number;
    constructor(private eventMarkerService: EventMarkerService) {
        if (Array.isArray(eventMarkerService.currentEvent)){
            this.multiEvents = eventMarkerService.currentEvent;
            this.eData = eventMarkerService.currentEvent[0].getData().event;
            this.multipleEvents = true;
            this.key = 0;
        } else {
            this.eData = eventMarkerService.currentEvent.event;
            this.multipleEvents = false;
        }
    }

    getImage () {
        return this.getEvent('img');
    }

    getTitle () {
        return this.getEvent('title');
    }

    getDescription () {
        return this.getEvent('description');
    }

    getEvent (key: string, defaultVal?) {
        return this.eData && this.eData[key] || defaultVal || '';
    }

    nextLeft(){
        this.key--;
        if (this.key < 0){
            this.key = this.multiEvents - 1;
        }
        this.eData = this.multiEvents[this.key].getData().event;
    }

    nextRight(){
        this.key++;
        if (this.key >= this.multiEvents.length){
            this.key = 0;
        }
        this.eData = this.multiEvents[this.key].getData().event;
    }
}


