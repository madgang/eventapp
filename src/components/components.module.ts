import { NgModule } from '@angular/core';
import { EventMarkerComponent } from './event-marker_temp/event-marker';
import { EventMapComponent } from './event-map/event-map';
@NgModule({
	declarations: [EventMarkerComponent,
    EventMapComponent],
	imports: [],
	exports: [EventMarkerComponent,
    EventMapComponent]
})
export class ComponentsModule {}
