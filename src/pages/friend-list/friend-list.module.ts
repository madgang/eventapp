import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FriendListPage } from './friend-list';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FriendListPage,
  ],
  imports: [
      IonicPageModule.forChild(FriendListPage),
      TranslateModule.forChild()
  ],
})
export class FriendListPageModule {}
