import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {FireStoreProvider} from "../../providers/fire-store/fire-store";
import * as moment from 'moment';
import {FirebaseFirestoreProvider} from "../../providers/firebase-firestore/firebase-firestore";
import firebase from 'firebase/app';
import {AngularFirestore} from "angularfire2/firestore";
import {FirebaseLoginProvider} from "../../providers/firebase-login/firebase-login";

/**
 * Generated class for the FriendListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

interface User {
    uid: string;
    displayName: string;
    photoURL: string;
    lastSeen: number;
}

@IonicPage()
@Component({
    selector: 'page-friend-list',
    templateUrl: 'friend-list.html',
    providers: [FireStoreProvider]
})

export class FriendListPage {

    userList: any;
    status: boolean = false;
    itemsDate: any;
    users: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private fs: FireStoreProvider,
                private fireStoreProvider: FirebaseFirestoreProvider,
                public db: AngularFirestore,
                public firebaseLoginProvider: FirebaseLoginProvider) {

        // fs.getUsers().subscribe(users => {
        //    this.itemsDate = users;
        // });

        //TODO change with true users id
        let storage = firebase.firestore()
            .collection('development/yuJ5CCJDP5yFDRWgMqQC/users')
            .where('fbFriends.' + this.firebaseLoginProvider.user.providerData[0].uid, '==', true)
            .limit(30);

        storage.get().then((snap)=>{
            console.log(snap, 'snap friends');
            this.users = snap.docs;
        });

        //this.db.collection()
    }

    checkPhoto(photo) {
        //  return new Promise(function(resolve) {
        if (photo == null || photo == "") {
            return 'https://i0.wp.com/circuits.io/assets/circuits-default-gravatar.png?ssl=1';
        }
        else {
            return photo;
        }
    }

    checkAvailability(userDate) {
        //  var currentDate = new Date().toISOString();
        var us = moment(userDate).format('YYYY-MM-DD HH:mm:ss');
        var duration = moment.duration(moment().diff(us));
        var hours = duration.asMinutes();
        return hours;
    }

    onItemClick(user) {

    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad FriendListPage');
    }

    isUserOnline(user) {
        let time = new Date(user.data().lastSeen).getTime() + 1200000;
        return time > new Date().getTime();
    }
}
