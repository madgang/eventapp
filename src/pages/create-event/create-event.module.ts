import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateEventPage } from './create-event';

import { TranslateModule } from '@ngx-translate/core';

import { Camera } from '@ionic-native/camera';
import {DatePicker} from "@ionic-native/date-picker";

@NgModule({
    declarations: [
        CreateEventPage
    ],
    imports: [
        IonicPageModule.forChild(CreateEventPage),
        TranslateModule.forChild()
    ],
    providers: [
        Camera,
        DatePicker
    ]
})
export class CreateEventPageModule {}
