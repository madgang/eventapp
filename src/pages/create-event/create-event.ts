import {Component} from "@angular/core";
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActionSheetController, AlertController, IonicPage, LoadingController, NavController} from 'ionic-angular';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {File} from '@ionic-native/file';

import firebase from 'firebase/app';
import {TranslateService} from '@ngx-translate/core';

import {ErrorsProvider} from '../../providers/errors/errors';
import {CreateEventService} from "../../providers/create-event-service";
import {DescriptionValidator} from '../../validators/description';
import {FirebaseFirestoreProvider} from '../../providers/firebase-firestore/firebase-firestore';
import {FirebaseStorageProvider} from '../../providers/firebase-storage/firebase-storage';
import {TitleValidator} from '../../validators/title';
import {ToastProvider} from '../../providers/toast/toast';
import {MapService} from "../map/map.service";
import {DatePicker} from "@ionic-native/date-picker";

@IonicPage()
@Component({
    selector: 'page-create-event',
    templateUrl: 'create-event.html'
})

export class CreateEventPage {

    private form: FormGroup;
    private locationValue: String;
    private picture: string;
    private dateTest: object = {};
    private nowTime: string;
    private maxTime: string;
    private startTime: string;
    private endTime: string;
    private timePicker: any = {
        startTime: {
            min: () => new Date().getTime(),
            time: ''
        },
        endTime: {
            min: () => {
                return new Date(this.timePicker.startTime.time).getTime()
            },
            max: () => {
                return new Date(new Date(this.timePicker.startTime.time).getTime() + 604800000).getTime()
            },
            time: ''
        }
    };

    constructor(
        private actionSheetCtrl: ActionSheetController,
        private camera: Camera,
        private createEventService: CreateEventService,
        private errorsProvider: ErrorsProvider,
        private file: File,
        private firebaseFirestoreProvider: FirebaseFirestoreProvider,
        private firebaseStorageProvider: FirebaseStorageProvider,
        private formBuilder: FormBuilder,
        private loadingCtrl: LoadingController,
        private navCtrl: NavController,
        private toastProvider: ToastProvider,
        private translateService: TranslateService,
        private mapService: MapService,
        private datePicker: DatePicker,
        public alert: AlertController
    ) {
        this.form = formBuilder.group({
            title: [
                '',
                TitleValidator.isValid()
            ],
            description: [
                '',
                DescriptionValidator.isValid()
            ]
        });

    }

    addEvent(): void {
        const loading = this.loadingCtrl.create();
        loading.present();
        this.firebaseFirestoreProvider.add(
            '/events',
            {
                event: {
                    creationDate: new Date(),
                    description: this.form.value.description,
                    title: this.form.value.title,
                    img: this.picture || '',
                    time: {
                        startTime: new Date(this.timePicker.startTime.time).getTime(),
                        endTime: new Date(this.timePicker.endTime.time).getTime()
                    }
                },
                user: {
                    uid: this.firebaseFirestoreProvider.currentUser().uid
                },
                location: this.createEventService.location || this.mapService.getCurrentLcoationLatLng(),
                locationQuery: this.createEventService.query
            }
        ).then((documentReference: firebase.firestore.DocumentReference) => {
            let eventId = documentReference.id;
            documentReference.update({
                    eid:eventId
            });
            this.firebaseFirestoreProvider.addCollectionCurrentUser(
                '/createdEvents',
                {
                    startTime: new Date(this.timePicker.startTime.time).getTime(),
                    id: eventId
                }
            );
            console.log(documentReference);

            this.navCtrl.pop()
                .then(() => {
                    loading.dismiss()
                        .then(() => {
                            this.toastProvider.presentInfoToast('ADD_EVENT_SUCCESS');
                        });
                });
        }).catch((error: any) => {
            this.navCtrl.pop()
                .then(() => {
                    loading.dismiss()
                        .then(() => {
                            this.toastProvider.presentErrorToast('ADD_EVENT_ERROR');
                            this.errorsProvider.setUserError(error, 'CreateEventPage.addEvent().add()');
                        });
                });
        });
    }

    getGenericCameraOptions() {
        return {
            quality: 50, //EDIT
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            allowEdit: true
        };
    }

    //TODO ask user about accessing photos
    openCamera(): void {
        let options: CameraOptions = this.getGenericCameraOptions();
        options.sourceType = this.camera.PictureSourceType.CAMERA;
        this.retrievePicture(options);
    }

    openPhotoLibrary(): void {
        let options: CameraOptions = this.getGenericCameraOptions();
        options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        this.retrievePicture(options);
    }

    retrievePicture(options: any) {
        this.camera.getPicture(options)
            .then((response: string) => {
                this.picture = this.fixPictureStr(response);
            })
            .catch((error: any) => {
                //TODO: add a proper error handling
                console.error('KLAIDA: ' + error)
            });
    }

    fixPictureStr(picture) {
        return 'data:image/jpeg;base64,' + picture;
    }

    presentActionSheet() {
        let translation: string[];

        this.translateService.get([
            'ADD_EVENT_PHOTO',
            'PHOTO_LIBRARY',
            'CAMERA',
            'CANCEL'
        ])
            .subscribe((value: string[]) => {
                translation = value;
            });

        const actionSheet = this.actionSheetCtrl.create({
            title: translation['ADD_EVENT_PHOTO'],
            buttons: [
                {
                    text: translation['PHOTO_LIBRARY'],
                    icon: 'images',
                    handler: () => {
                        this.openPhotoLibrary();
                    }
                },
                {
                    text: translation['CAMERA'],
                    icon: 'camera',
                    handler: () => {
                        this.openCamera();
                    }
                },
                {
                    text: translation['CANCEL'],
                    role: 'cancel',
                    icon: 'close'
                }
            ]
        });

        actionSheet.present().catch((err) => {
            console.error(err);
        });
    }

    // Masė:

    openLocationPage() {
        this.navCtrl.push('SetLocationPage');
    }


    getLocationValue(): String {
        return this.createEventService.setLocationValue || 'Set Location';
    }

    setLocationValue(value: String): void {
        this.locationValue = value;
    }

    openDatePicker(timeMethod: string) {
        if (timeMethod == 'startTime' && this.timePicker.startTime.time && this.timePicker.endTime.time) {
            let alert = this.alert.create({
                title: 'Editing start time will reset end-time',
                message: 'Continue',
                buttons: [{
                    text: "Yes",
                    handler: () => {
                        this.timePicker.endTime.time = '';
                        this.openDatePicker(timeMethod);
                    }
                }, {
                    text: "Cancel",
                    role: 'cancel'
                }]
            });
            alert.present();
            return;
        }


        this.datePicker.show({
            date: new Date(),
            mode: 'datetime',
            androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
            is24Hour: true,
            minDate: this.timePicker[timeMethod].min && this.timePicker[timeMethod].min() || '',
            maxDate: this.timePicker[timeMethod].max && this.timePicker[timeMethod].max() || ''
        }).then(
            date => {
                this.timePicker[timeMethod].time = date;
            },
            err => console.log('Error occurred while getting date: ', err)
        );
    }
}
