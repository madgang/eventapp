import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewsfeedPage } from './newsfeed';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    NewsfeedPage
  ],
  imports: [
    IonicPageModule.forChild(NewsfeedPage),
    TranslateModule.forChild()
  ]
})
export class NewsfeedPageModule {}
