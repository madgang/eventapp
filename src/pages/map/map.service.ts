import {Injectable} from '@angular/core';
import {ILatLng} from "@ionic-native/google-maps";
import {UserService} from "../../providers/userService";
import {FireStoreProvider} from "../../providers/fire-store/fire-store";
import {FirebaseLoginProvider} from "../../providers/firebase-login/firebase-login";
import {BackgroundGeolocation, BackgroundGeolocationResponse} from "@ionic-native/background-geolocation";
import {Observable} from "rxjs/Observable";
import {FireEventService} from "../../providers/fire.event.service";


/**
 *  domMarker https://developer.here.com/documentation/maps/topics_api/h-map-dommarker.html
 */

@Injectable()
export class MapService {
    map: any;
    coords: any;
    markerCollection = {};
    coordsSetByUser: ILatLng;
    public location: any;
    public locationService: Observable<BackgroundGeolocationResponse>;

    constructor(public user: UserService,
                public fbs: FireStoreProvider,
                public fireBaseLogin: FirebaseLoginProvider,
                public backgroundGeolocation: BackgroundGeolocation,
                public fsEvents: FireEventService
    ) {
        this.user = user;
    }

    setTrackingPosition(resp: any) {
        this.coords = resp;
        this.coordsSetByUser = {lng: this.coords.longitude, lat: this.coords.latitude};
        this.fbs.setLocation({lng: this.coords.longitude, lat: this.coords.latitude});
    }

    public getCurrentLocation() {
        return this.location;
    }

    public getCurrentLcoationLatLng() {
        if (this.location && this.location.longitude) {
            return {lat: this.location.longitude, lng: this.location.latitude}
        } else {
            return this.location;
        }
    }

    public getLocationSubscriber(): Observable<BackgroundGeolocationResponse> {
        return this.locationService;
    }
}
