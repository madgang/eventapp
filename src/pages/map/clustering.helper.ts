import {Component, Injectable} from '@angular/core';
import {HereMap} from "../../providers/here-map-service";
import {DomService} from "../../providers/inject.service";
import {EventMarkerComponent} from "../../components/event-marker/event-marker";
import {Events, ModalController, NavParams, ViewController} from "ionic-angular";
import {EventMarkerService} from "../../providers/eventMarkerService";
import {TabService} from "../../providers/tab.service";

declare var document: any;

@Injectable()
export class MapClustering {
    bubble: any;
    ui: any;
    map: any;


    constructor(private hereMap: HereMap,
                private injectService: DomService,
                public events: Events,
                public modalCtrl: ModalController,
                private eventMarkerService: EventMarkerService, private tabService: TabService) {

    }

    getCustomTheme() {
        let H = this.hereMap.H;
        return {
            getClusterPresentation: (cluster) => {
                // Get random DataPoint from our cluster
                // var randomDataPoint = this.getAllDataPoints(cluster);


                // Get a reference to data object that DataPoint holds
                let data = this.getAllDataPoints(cluster);

                //this.injectService.appendComponentToBody(EventMarkerComponent, target);

                let domIconOptions = {
                    html: `<div   class="event-marker-wrapper">
                                    <div   class="event-marker event-marker-count-wrapper">
                                        <div  class="event-count-marker">
                                            ${data.length}
                                        </div>
                                    </div>
                                </div>`
                };

                let icon = this.hereMap.domIcon(domIconOptions);

                // Create a marker from a random point in the cluster
                var clusterMarker = new H.map.DomMarker(cluster.getPosition(), {
                    icon: icon,

                    // Set min/max zoom with values from the cluster,
                    // otherwise clusters will be shown at all zoom levels:
                    min: cluster.getMinZoom(),
                    max: cluster.getMaxZoom()
                });


                // Link data from the random point from the cluster to the marker,
                // to make it accessible inside onMarkerClick
                clusterMarker.setData(data);

                /*if(!document.getElementById('testcluster')){
                    this.injectService.appendComponentToBody(EventMarkerComponent, clusterMarker.icon.i);
                }*/

                return clusterMarker;
            },
            getNoisePresentation: (noisePoint) => {
                // Get a reference to data object our noise points
                var data = noisePoint.getData();

                let domIconOptions = {
                    html: `<div class="event-marker-wrapper">
                        <img src="${data.event.img}" width="50" height="50" class="event-marker">
                        </div>`
                };
                // Create a marker for the noisePoint
                var noiseMarker = new H.map.DomMarker(noisePoint.getPosition(), {
                    // Use min zoom from a noise point
                    // to show it correctly at certain zoom levels:
                    min: noisePoint.getMinZoom(),
                    icon: this.hereMap.domIcon(domIconOptions)
                });

                // Link a data from the point to the marker
                // to make it accessible inside onMarkerClick
                noiseMarker.setData(data);

                return noiseMarker;
            }
        }
    }

    startClustering(map, ui, getBubbleContent, data) {
        let H = this.hereMap.H;
        this.map = map;
        this.ui = map.ui;
        // First we need to create an array of DataPoint objects for the ClusterProvider

        var dataPoints = [];
        data.filter((item) => {
            // Note that we pass "null" as value for the "altitude"
            // Last argument is a reference to the original data to associate with our DataPoint
            // We will need it later on when handling events on the clusters/noise points for showing
            // details of that point
            if (item.location && item.location.lat && item.location.lng) {
                dataPoints.push(new H.clustering.DataPoint(item.location.lat, item.location.lng, null, item));
            }
        });

        // Create a clustering provider with a custom theme
        var clusteredDataProvider = new H.clustering.Provider(dataPoints, {
            clusteringOptions: {
                // Maximum radius of the neighborhood
                eps: 32,
                // minimum weight of points required to form a cluster
                minWeight: 2
            },
            theme: this.getCustomTheme()
        });
        // Note that we attach the event listener to the cluster provider, and not to
        // the individual markers
        clusteredDataProvider.addEventListener('tap', (w, e, f) => this.onMarkerClick(w, e, f));

        // Create a layer that will consume objects from our clustering provider
        var layer = new H.map.layer.ObjectLayer(clusteredDataProvider);

        // To make objects from clustering provider visible,
        // we need to add our layer to the map
        map.addLayer(layer);
    }

    onMarkerClick(e, w, f) {
        /*//TODO get event or events data and show ionic event winodow
        console.log(e);
        /!*let profileModal = this.modalCtrl.create(ModalPage, { userId: 8675309 });
        profileModal.present();*!/
        // Get position of the "clicked" marker
        var position = e.target.getPosition(),
            // Get the data associated with that marker
            data = e.target.getData(),
            // Merge default template with the data and get HTML
            bubbleContent = this.getBubbleContent(data),
            bubble = data.bubble;

        bubble = new this.hereMap.H.ui.InfoBubble(position, {
            content: bubbleContent
        });
        this.ui.addBubble(bubble);
        // Cache the bubble object
        data.bubble = bubble;
        */

        this.eventMarkerService.currentEvent = e.target.getData();

        if(document.getElementsByTagName('event-marker').length > 0){
            document.getElementsByTagName('event-marker')[0].remove();
        }
        this.injectService.appendComponentToBody(EventMarkerComponent, this.tabService.tabRef.getSelected()._elementRef.nativeElement);
    }

    getBubbleContent(data) {
        debugger;
        if (data.length > 0) {
            //TODO show template for multiple marker popup
        } else {
            //TODO show single marker popup

            return `<div class="single-marker-popup">
                            <div class="single-marker-img-wrapper">
                                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhUTERMVFhUXGBgXFxcVFxoYGRcbGSAYHh4aIBcbHSggGRonGx8dIjEhJiktLi8uGCAzODMsNyguLisBCgoKDg0OGxAQGy0jICUtLS4vLy8tLS0tLi01LS01LzcvLS0tLy8tLy0vLS0tLTUtLS0tNS0tLS0tLS0tLS0vLf/AABEIAJABXQMBEQACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAABgIDBAUHAQj/xABGEAACAQIDBQcBBQUGBAUFAAABAhEAAwQSIQUxQVFhBgcTInGBkTJCUqGx8BQjYoLBM3Ki0eHxJJKywlNjg7PSFRclNET/xAAbAQEAAgMBAQAAAAAAAAAAAAAAAwUBBAYCB//EADwRAAIBAgIGCQMDBAAGAwAAAAABAgMRBCEFEjFBUWEicYGRobHB0fATMuEGFCMzQlLxFWJygqKyJDRD/9oADAMBAAIRAxEAPwDuNAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAp3jXWgKWJBECQdDqPLodeusChlFyhgou3VXViBJAEmJJ0A9SdIolfYC3buMW+kBRI1PmzA8hplI1mZ6VlpW5mLu5frBkUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKATQCgFAKAUAoCzhg+UeIVL6yUBAiTGhJO6PevUtW/R2GFe2ZeryZPKAGgLbOcpygFhwmPkwY0orXzDLVt0u5vMlxVaIEHI6bwdT5gfQivbUocVl4P0POUuZkivB6PI5UBVQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoCO7f7b4HByL19c4+wnnYdCBop9SK26OBr1VeMbLi8l+ewilWhF2vmRod8+An+yxUc/DSP/cra/4RW/yj4+xH+5jwfh7kk2V24wOIjLeyk7hcBTf/ABHy+01BV0biaSu43XLP8iGMoylq62fPIkdaJsigFAKAUAoBQHgoATQFFhyVUsMpIBImYPETxistJOyMLYVg1gye0BS/CgPGXQgGDGhOse1EGVKKA9oC3h7CoMqiBJPuxJP4k1lybd2YSsXKwZPDQHooBQCgI53iX2TZ19kJBhFkGDDOikT1BI96hxDapSaNfFycaMmuBG+6i5cz31k+EqpC8MxJ1HAaAz7VpaO1ulwKvQznaV3kdHqzLwUAoBQCgFAKAUAoBQCgFAKAUBqdv9obODWbrSx+lF1ZvbgOp0rawuDq4mVoLre5GviMTToRvNnM9s9tsViCQjGynBbZg+9z6ifSB0rpcPonD0lea1nz2d3vcoK+lK1R2j0Vy295cwvZTaVwZvOuYfbuwSDzGafmvNTH4CDtZO3CP4PUMLjpK92utmrxfd3iBvwk9UKn/paaljpHBzz1u+5h4fHQ3PvTI3tHs0UOVhctt924p/IwY61tR1aivCSaI/3VSm7VI+hjYK1estDLmQ8VMx7b/wAK9wbTs9grSpVo3i7Mn/ZftpewwCMPFsjQKTDIP4W5D7p+RVdjdFU67c4dGXg+v38yXC6TnR6E814o6NsztThL5UW7wzMYCMCrTyg/7VzlbA4ijdzi7LftXeXtLFUav2yXVv7jdVqGwUW7gYSpBG7Qzuo8gYT2hfuKxHktHMhDEZn1E6GCoBI1mSemsqepFre+W73I7azvwNhURIKAUBReMD4G6d+lZRhldYMigFAUuBuPHSgKLCnWSTrpIGnTTfQFwUBYxWJVLbXIZ1CloQZmYcgo3mvUYuUlHzyPLkkrlVh5GilRpEiNIB+nevKDGorDVmZRVft5lKhisgjMsSJ4iQRNE7O+0NXVi5WDIoBQCgIx3kpOzr3Q2j8XLdQYlfxSNXGq9CXUanunxGa3fEQAyn/mzVraPfRaNPRL/jaJ7VgWwoBQCgFAKAUAoBQCgFAKAUBp+1O3Vwdk3Dq58ttfvN/8RvJ/qRW3g8LLE1VBbN74I18ViI0Keu+w4tj8a952uXWLMx1J/IcgOVdrRowpQUIKyRyVWrOrPWltOk9hezMAYq+gDNBt2wICDgxB+3rpOonmdOZ0njrv6NN3S2vjy6vPqL/R+D1f5ZrN7Fw/JOKpS2FAY+OwNq8uW9bR15OoI/Hca906s6b1oNp8jxOEZq0lc5t257uP2m2XwUqQ39i2gYglSQTuE666aSDVzh9KO2pXfb7lf+yVOf1KS5W9jmeHN/C3Th8RbZLgMZWBzHgN0yDwIn30i7oVoyhrJ3XE0MXhdZ3SzOo9idg4jCYnxcTY+pClshkIV2IMfVIOUb45iqnSONp4mjq05bHd7dncbeCwsqEryW7kdAGHa5rcby8EQmPd9C3poNdQd9UWtGP2979v9lpZvaW8Jse1aUogy2yS3hrCpJ36KASOhJFep15zetJ3fHeYjSjFWWzgZrsqqSSFUCSToABx6CKiSbdke9h7auBgGUgqQCCDIIO4g8RRpp2Zk9rAPaAtM0sBG4SenAcNePxWd1zG8oxOMVN8kyghVLHznKDAB0mZO4QSazGDls5+AbSPcVdZUZkXMwEhSSJ6SAx+AaRSckm/ngJNpXRZ2jjCiHw48Q5QuYHKC5hS0axP5GvNpP7dpJT1NZOps5bewv2c0y0RA0A46yZnUboED3nRuPD25FeU6xG7TTjrrv1FDAZASCQJG48ppcyFSP1+tZoBk0iaAaz68OVAVa0ABoClQQTJmTppuEDTrrJ96GCuaGSPdv0zbPxA/hU/DKf6VDiP6Uuo18V/Rn1M0fdHYixefncC/wDKoP8A3Vq6OXQk+foaOiF/HJ8/QntWBbCgFAKAUAoBQCgFAKAUAoBQHE+222zisS5B/d2yUtjhA3t/MdfTLyrtdF4T6FBX2vN/ORy2kMQ61VpbFkjP7vtiDE3czrNq0Q7T9tvsqeg1J9uda+lcW6MLRfSlkuS3v0/0SaNwv1J60ti8zrtckdKUu4G/mB7mlhcqoBQFGgPr05dfegNZt3YVnFZDdtgujBkuDRrbLqCDxGYDynQ8amo150rqLye1cSOdNSae/iY13aN0L4d+wbhJVQ9pDctPJAzECSkbyGgaaNxqT6UX0qcrcm7Ne/Z3HjXf2yXatn47e8vjA3E1S/cCg6KyrcUyABv88BtdGGkjrXj6kZLOK8vx4HrVktj+efiXcVtcWLT3MT5QgklZKvuAy9STGU6zzGpgquMFrXy8TZw1GpXqKlFdJ/LnNts94uIuSLKi2hkbgzEHmTpPoKrpYud+jkdjh/05h4x/mbk+5eGZiYXvMxduM+RxyZI06ZYikcRV4pk1X9PYOS6N49T97kx7Pd4mHxBCMvhXT9lm8rf3XIHm6GBrvrZp4iMnaWXkc/jtBV8OnKn048tvd7eBK7FzJbl3zsCQSoAJMmFyjiN0VttXlkrFEnZZnly54a52JnUlVElidygcSN3415lJI904OTsY+z8Xeu5jcsC0mmUO8uRrJZAsLw0zHjMbqji5PNqxPWp0YZRnrPfZWXY3t7irH41VUlrmm4BNGJ9Z/ERWrjcbSwkb1Hm9iW1mKFN1JdBd+w1S7SeItmBGrMcxPu39BXM4j9QV5yao2iu9+PsbywkI5zzfzgVW9sOv1MT/ACq35RTD6dqJr6s2+yL9jMsHCX2rxa9zaYLaa3IysrHiPpPspmfmuhwukadeyjJN8PtfYne/eaVbDSp7U14rv/BmWiTrDLJ1DfHUR6VYQk5RTatyf4NeSs9pXrXoweXHCgsxAAEknQADjNZSbdkYI9hdtrirk4K2txVJH7TckWwdARaH1XNwnLC9TNbs8K6Ef53Zv+1be3cuV7vkQ6+tLoLtfoYO1dn7ZXz4fG4e4d5tvh/DB6Bs7H0kjqa9wqYGWUoSjz1r+iPTVRc/A0WE7yb9m6bGOw4W4uhiUPrBLAg8CDB51Yy0NSqw16E8u/2aK6ePq0v6kO75mTvZe27WJTPafT7U6FDyYHdoDrVLXwtShLVmvz1G/QxFOtHWgy8ri60j6UkgnixBEiRoApOo35vWo2nBWe/y+eRLtNX20YDA4gTPkB4new47vbhWtiE3SklwZBiv6E+pmr7pz/wt0f8AnH/ot1rYBWhJLj6I09Ef0X1+iJhi8UlpS911RBvZyFA9zW65JK7LinTnUlqwTb4I1l/bhyl7VlygE+JdIsW/l/PHXLFRuo9y78vz4G1HCRvac1fhHpPwy/8AI1C4za1/zWreHtJvXxDck+zIG+VWor15ZqyXabv09GUujNzk+WrbwdvFkf2623rQL+LKgT+5to4HSMmf3iOtRy/cLN+H+jfw/wDweq9VRs/+ZteOtbyIknbrawP/AOwD0Nu3r/hqL9xPj5Fq9B4Z/wD5rvl7kw7D94V+/fWxiVQ59FZQVbN1EkH2jjU9KvJytIqtKaDpUaDrUm1banmrfOs6bW4csKAUAoBQCgNZ2lxhs4W/cUwwttlP8REL/iIrYwtNVK0IvY2u7eQ4iepSlLgmcEtDM0bhPwK76XRjc5Fo7R3c4Pw8DbMQbha4fcwv+ALXF6Wqa+KkuFl7+Nzp9H09SgueZJqrTdKLhge4oYZXQyKA8oDxJgZonjG6jB4E1nXdETp8c6XMWPQwmOP+X+9DJy7vK2p4t9cKn0WRmYDQF4n/AApu6seVV2MqNy1VuOx/T2EVKi8RJZyyXVf1fkRLBbPuXnyWlzHSeAA3SSdFHrWpTpym9WKuX2IxVLD0/qVZWXzZxJd/9uGVA+Iuk7sy2Lecr11ILD0Welb9LAOTs5JP5vOcxH6ojH+lTuuLfovdljbXdk1pC1m6LsCRaKZXMb8rBtTHCKxPCS1bxdz3h/1JCc1GtHVT3p7Oyxm9le1jJhrovk3LmHtlrWbTOPpyt/GCQpPI+tKGKtBxe1fLfPQg0noeM8TCpSyjN2dtifFda+Zm37vcPecXcbiWLXLpypPBF3wD9ILcN3lHOpMMm71JbWaWmZ0qbjhaKtGOb5vnxsvM3+1MWAhVgrFiVCkTAiDmEkHj8gVr6T0l+yitTOb2e/zeVWHofVfS2LaY2ztm+LL32LGWWJ3QYMxuMjdVXgtFyxDlVxbu72tfhxtw3JZbzbrYn6do0lYycTsVT9BZT65h7g6/BrZr/p7CTXQvF9bfn7kUMbNfcr+BpcVhXQw4g8I3H0NcnjdH1sHK1RZbnuZY0q0Jq8TVY0Earo41B9Khoys0bkFdWewmHZvaHj2FfiQM3OevXh7V3mi8VKvRam7yi7Pnwfb53OfxdH6VRpbDYX7qorO5CqoJJJgADUk1aRi5NRWbZqtpK7OObS25d25jVwlrMuDViWA0N0JqWadI+6u6SpM8OqpYSGjcO69TOe7k38z7TQnW+tJQjv8AI7BgcItm2ttBCoIUQBAG4aCuXqTlUk5y2s34xUVZF8V4MmFtHZ6XfrA0GhKgkHXnwidORPOpIVHDYeJRucgx15tm7Qbw58MMB/etuAwgniJyg9Aa66lBY7BrW2+q995z81+1xD1Nm3s3r26jrtrIVW6GLgiUJ10YDdxI3b5NclJSTcGrcewv000pXND2+dlwF2Yl2tj/ABJI66Ax0GtamMlak9X5mauPbWGmWe66yRhHO7NeYiOQCD8wRUGAVqbfFsh0TFqhfi36EmfZVlrwvsua4oygsxIXqEJyq3UCa29Ra2tvLpYioqX0k+jtfPre3sMXAp+0v47jyIzCwp3QNDdI3FiQcp4Lu+o14h0uk+z3+biWt/AvpR22Wt18Ozfz6kbipTUFAcz709jW0a3iEAUuStzgCYkMeRgGTx0qvxlNJqS3nXfpzGTcZUZZpK65cV5W7TQ92+xmv41bw/s7JzFogEwQoHUnX0HWvOGhrS6jf09i40sK6b+6eSXLe/TrO1VZHBCgFAKAUAoCPdvwf2C9H/ln2FxCfwrf0Zb91C/PyZp4+/7aduBw1ePrXcu285lneuyF5Tg8OAQStizIB1Eop1HCuBxqf7ibf+UvNnWYf+lFcl5G5rVJizcvplJLqAN5JECOZnnWVF3tYw2rFhscTPhW2eOOirPKWOvqARrXtQ/ydjzr8Fc5vj+9Nbdy7bfC30MsrxcVmVh5TlmAhEcJGkxqSbdaKvFSUk+z5fzNNYvpNNW7SvAd7eBJCv8AtmHH3mC3l9zLv8CoKmAqxztF+HlZE8akXkn87SR9oMXZbAtisPiXYx+6u27zQXJAAyocp13rGkHTSvGDjKWIVJxVt6stnW8/EjxTjCk5uTy57/IwOxPbM3R4OJMXZ8rmAHJ4awobkOPrv2dI6MdL+Sn9u9cPwa2Cx6qdCbz3Pj+SbiCIJPmG7UescRv3+lU2ws8jiF9Wv4q4RmJe624T9TwB6SQKo53lUfX6n0uk40MJDdaK8Fn6s7JsPY1rCW/DtLHFmP1MeZPH04CrenTjTVkcBi8ZVxVTXqPqW5LkZtwzBDQOJ0/ruqVGoynJB8o10ksSZB5HX4pe5ixzzvB2Ulm7+0KJW8GV03ecCc27cQNRzHU1X4qCi9fidZoTFTqwdB/22afK+z2/0Srsnfz4HD3HJnwxMfaPOOJkT71s0p2pJv5/so9JUlHGVIrYm/nYW7F0X8QTH0k6cysaekmSelc3Rl+80i6sllBdFc93jd8iScPo0EuPr8sbnDWCAwk/WTO7fB4fl8zXRUoNay5+iNGbTt1GXWwRlnF4cXFyt/sedQ4ihCvTdOaume4TcJayIXtjDlQ071kH9cuPoa+d1sPLDV3RluOgwtVS2by93e3jFy3yJI94P9TXQ6FqWxUo/wCUb9qdvJmrpSGSkabvZ2+VC4RDpAe6ef3V/DMf5a+naBwWtevJcl6v07zkNI1nlSW/aa3uVsD9ovN920Av8zST6kgfh0rY/UUmqUY8ZeS/I0er1JS3nXVO/wDUVyRbHtAUcuvEbqGDkfetb/4yBvNlD7y/+QrrtBP/AOM/+p+SOf0o7YiL5erJ52EvZsBhydJWJ3fSWA/AVQaTjq4ua5+iLbB/0Imn718RFmzbnVrhf2RSD7S4qh0hK1NLizU0vO1FR4s3vYfC+HgrAnepc9c5LD4BqfCR1aMe828DDUw8Fyv3mdt7GCzYuXDoYyg8ZY5R+JqWpJRjdlnhaLq1VFLn2LNmdYQKoVRAAgDkBoBXu1siBycnd7WVzQweE0Bz/tbs+9tTEWrWHIGGtSbl7ehcmCF++VAjTSSQSK1KkXVlaOxbzosBiKejqMp1F/JLZHfbdfhe/XazRM9i7Jt4W0tqyIUcTvY8WJ4n/bdWxCChHVRS4rE1MTUdSo834ckZ1ezXKXblxoDzjQFdAKAUBgbcwnjWLtkb3tsAeAMafjFTYep9OrGfBojrQ16cocUz57XRypkdOOn9a+gXusjkmmkdk7sscLuCVdM9hmtN7aqfQoV+Dyri9LUnTxMuDz+dtzpsFNToxaJT5jG753744VXZG1mUW8GobNCzuJCgekHfpJ4/aNZc21bcYUVe5TjL9u2DcuXBbX7xYAc+OhP6FZhCU3qxV2Yk1FXbscJ2zsXF4/E3r2Fwlw23uMUZlCBhP1S0DXf711FOtTw9KNOpJJpZlS6bq1HOGaZtuz/c7cuQ2Ou+HOvhW4ZuG99w37hPrWhX0lBO0FfwXv5G3DDyazy+fOJI9t9gMPhMExwwYNb/AHjSxhwB5jG4EDUGJ0j0YDSEniFGSSUssls4Z7TXx2D1qN1JtrPP22EC994+en65V0xzayOq92m13vWHt3GLNaYAE6nKw8oJ4wQw9IrlNMYaNGspRVlJeO86jRleVWlaTu15Gn7IbOVdo3wx1RrjIn3vOQG5EKCD/MDwrmKFP+aV9x3mlMU3o+lqr7kk32LLt8kzottp1iOh3/6VYHKI9AoZPaAh3b/Z1y5kuFk8C2CXWSHknWNIPl6iINaONjLUutxeaGxEKblBJ68sk93b29Zf7F+XCMF08J7yBT9nzFoGp01jjpFRqUo0JVN8VLssRaUi3iVf+5RfXlb07y12LxmZ3B45vlWP9Pyqo0DJRqzg9rin87zOkqfQjJbiVqTPtqCd3sP1pXTZ3v8APnaVGRcmvZgL1oDRdpsOCJ5qQf5dR+E/hXMfqLDroVltvb1XdmWOj6lpW+fNhpexKxecdB/WtTQ8r4uPVL0N3Sf9JEG73cO1vaBYjy3raMp4EoMpHroDX2jQNWMsKo7037nEY6n/ACaxl9zN/LjLiH7dkx6qyn8ifiov1FC+HjLhLzTM6Pl/I1yOxbpMa+363VxxblUmd2lDJ6TQHGu8jGC5jbkbraqnuBJ+CxHtXZaHpuGFV97b+dxzOk5qeItwSR0Ts5ct4bCWrNy4itbtrnUsJUnzNI4QTXN4xTr15VIptN5ZbeBe0HGnTUW80iFd5uMz4hFBlUtAj1ckn/CFrm9JS6ajwXzyKfTE71Iw5X7/APR1HCWAltEG5VVfgAVaxVkkX8Y6sUiO9u748C2N6+Pbzxr5QST+IHzWviftXWi40TH+WfHUlbrsbw42ZCpcPKFABHRmIH41Prciu+lZXbXf6LMj+NubSvPFpBYtkCCzJmE780Zjm6DTqa15/Xk+jki0orR1KF6jc5ck7dmzLr7kVYfsizz+2Ym7fH3AzonUEZtR8Ujhm/6kmxPS0Yf/AFqcYc7Jvyy8STYewttQiKFVRAVRAAHACtlJJWRUTnKcnKTu3tZcrJ5KWaKAIOf+1AeNp/p+dAV0AoBQHjCaA4p3m9njhb4v2li0+oCjRWA8y9J3j1PKuu0VjPrUtRvpR8V8yKLGYdQqcpeDLPYPtCMLiA5aLN0BLvSJyv8AykkHozchUmlcH+5pa0PuWzmuBFgMQ6FT6c9j8zuCNIkEEHUEcq406IxcXiHDBLa6kSXb6VE8gZJPAbt5J019xirXZ5k3sRiLspCy3LnnucC5LgSQTlB0TQcI999SfXkk4xyXLLv49pG6MW1KWb7+42evz+VQEpWBQyR/t7jfCwN3dNweGJ459/wmY+1b+jKeviocs+782NTHVNShJ8rd5w67iIgDjurt20jl4075s653WbMa1Ye64jxSMoPFVmD7kn4rk9N141KyhH+3zZfaJpOFJye/yNngLVu3tPEcHuWbTqOYBdXI6yFn2rnopKrLi0jp60pzwNLhGUl25Netu0kdTlaKA8AigND2wIXB3zvlYnTiQI+fzrWxCSpyfIsdFpyxdNLj6Gr7tQz4e+Wk5rx1PHyWwT/T2qDCU9ajKL2O/ije0/qwxEFHdFf+zNNsljZxV62R9L5wOYacw/Ej3rlKdWWFq06r/terLsyZJVSrUbcUdHtEEAqRBAIjlXcQcXFOOzcc60089pcAr2YBoDR9ppIWN0OP5iBA+M1c7+ooydGD/tTz693qWGj2lJ9ndv8AQ1/Y3DxcuNwhR7nWtPQENau58I+by8mbOk59GMTL7bdmF2hY8MkK6+a08aq/r90jQj/Ku8wGNlhKuutj2rl7o5+vS+pG2/588dxx7Avc2ffUspXEW2kqwIyxoRMkPbdTvEV2NRRx1JpPoNbt/s09xQNzoVVLh8+eB3PYu07eKtJftGVYbjvVhoVPUbv8wa4avQnQqOnPajo6c1UipR2Mz4mP1/vUJ7NdtvalvD2Ll26QVUEgH7RgwvUk6e9T4ehKtVjTjtfhzI6tRQi5M5T2E2McdiTdvea2pz3CftsZIXrJ1PQHnXV6UxSwtH6dPJvJclx9uZQ4PDfXq6080tvN/M2djtWFUZVUKvAKAAPYVx0pOTu8zoUklZHIu191WxN1hq/itPQW4RV3/wABPuKo8XKP1uZy+kZx/c5bfxs82dbUZxMwDyP4zV1tOqTNF27wc7OvhZJVRc1Mk5GDtqeYB+ahxEb0miy0RV1cbTb3u3erIudiNupjMKjKwzqAt0biGGhaPutBIP8AUGvVGevFMj0lhHhsRKO6+XV+N5IKlNAUAoBQFgPJn1AHODE/rnWD00Vy3QVk85FQXnQFVAKAUAoDC2xsu3ibTWrolW48VPBgeBFTUK86FRTg80RVaUasHGRwbtL2fvbOvEMua2dQQNCPvDrzXh8GuyweLhXhrw7VvTOfr0JRf06m3c+JI+x3bd8Kq23Bu4f7MfXaH8M/Uv8ACYjgdIrSx+io126lLKXg/Zk+F0i6f8dbvOnbLx9nEW/Gs3BcDHep3clIOqnoY1J51zNWnOnLUmrNF1GSktZO5nWl5jh7fFRs9ovARWDJ7QHJe9vbBuXkw9oyLQJc8M7cPVVH+M10+hMM4wdVrbkur8+hS6TrRclTvszfXuMHu97EtiHGIxAPhA6TpnjgOnM+w4xLpLSCoJwg7zfh+fnXFhMM67TatBeL9vnV2dFAAAEAaADcK5O99pf7CB95Fq7ZuWMbZMNb8p0nnEjipBIPrWlidaMlUXUdHoN0q1OphKv92fzmsmiTdmdvW8ZZFxNG0DpM5D68VO8Hj8gbNKoqkboqMdgp4Sq6c+x8V82m3qQ0xQEE7wtoeJGGtEeUh7xnReCqepMmN/lFaGLnrdBdp0eg6Cp3xFRbco8+L9L8ySdm8AMNhrdvjvbnmYyfjd7Vs0oqnBRKjH4h4ivKp3dS2e5pu1OzCLq4m2OGV4/D9da5rTeE1W6q+2W3lL8rx6zc0fXTX0pdhkbE2sFEHVT8qfTl0rW0Vpd4b+Gv9m58PdGMXhNZ60dvmSVXDAEag8RXZRnGUVKLumVLTTswGk8dK9GDV7cvRlQb/qP5D5J/Cub/AFJXtRjRjtk/Bfk3cHDNyL2xsH4SZSPMTJ9/8t3tVlovBLC0FF/c837dnuRYqv8AVqX3bEZ5NWNzXNL2k7M2MZbAuiGUQlwfUvvxHQ/hvrdwePq4WV4PJ7VuZr4jDQrRtLbxIbsvZGP2W7Pay37BkuucKCB9rzHytHETpvnSLivicJj4pTvCe7K/ZltXcV1OniMK+j0o9djabW7feDbJ/ZriudFLsptyIkhlJLATrA9xWpQ0T9SX3q2+17+KRPW0goL7Xd7L29yCbPF/bOL8N7zZVVrjGJVVUgQiyFBYkAa9dY1u6rpaNoa0Yq97dfW9vyxq0adTETvUb+cFuOy7E2RawtpbVpYA48WJ3knif8gNBFcliMRUr1HUm8y4pUo0o6sUZqJBJ4mJ3xpyHCoD3Y4r2rfw8dfXj4hb/mhh+dUGLTVeTXzI5TSMGsTJrl5IlfZTt+nlsYsqhiFu7lMcG+6eu70rew2Mv0Z5cy0wWkdZatXLmT91DKQYKsIPIg1YbUXMZNNNHBkxt7Y+OuWQTlViUnijajlIIiV5jeCARXdKlLI7eNKjpGinLer9u9dj2ctzTsdd7NdrMPjFAVgtzjbYwfVZ+peo9wK3KdaM+s5jHaLr4R9JXjxWzt4P4rm/qYri1icQttS9xgqqJLMYA96w2krs9QhKclGCu3uRqsHtJcaGFrMLIOVrn0l+JVOIEaFtCJgQdRFGaqfbsNyrh5YNr6ltbbbbbr9Fs3vLJ7e1aCiFAA6fH5VKlY0m23dldZMCgFAKAUAoBQGLtLZ1vEIbd5AyngeB5g7weoqWjWnRlrwdmR1aUKkdWaujmO2+7O7aYvgmzodTbaA3tuBPXT0NdHhdNU5ZVlqvitnuimxOjZ26PS8/Z+BF3wuIw1zOvjYe7ukZkJ6H7w6GRVpJUMTGztJd5XQrV8M7O6Jd2c7w71sBMfba4v2b9pRP86aA/wB5fjjVHitDZ3oPsZcUNK05R/kyZJm7xMABo9wnkLNz+qgVorRGL/x8V7my9IYf/JGBiO2d/FIy7Pwl7MfKLlwKoX+IAE+0ka1PDRsKMk8TNJcN7+ciGpjpVI2oRbfG2S9PnYWOzfd6Qwu45gxmfDWSCf42O/XgN/PhU2L0z0dTD5Lj7L51EGH0X0tes78vdnQEQKAAAABAA0AHKKoW23dlykkrIqrBk0Ha1h/w6kTmvqsESCMrEgjlpUFb+1cyx0df+Rp2tFvxRH8d2QxGEunE7LcD71l9VYbyNSJHuD1JqJ0JU5a1Pu+fOZY09KUcVT+jjF/3L5k+q64osN3kXrRyYnAsrjlcIn+Vkn86PF2yase4fp+NXpU6qa42v5P0NdtPvDxN793Ytizm08pL3D0BgAH0BPpUE8XOWUcvFlhh/wBP4ej06sta3Hort/2kbnsT2SuAi/ipEHOlpj9r/wARh97lx51Lh8O10p9xX6X0rCadHD7LWcuXBcvDgTyt6xzRQbIIYNqDwMaCAI689edeZwjOLjJXT28zMW07oju0NhlDmtSRxHEf51x2kdCVKV50elHhvXuvHzLahjlJatTaW8FinT6TpxU7vz0PWq/AaSxGFlqU81wez8HutRhNdJdpsDt3KCTb0G8hhH5Vex/UMpZqjktr1su+xqLA3dlLwPUtXGY3cigmIzn6Rw0j+u8mvf0MXWqfulCN39qk/tXVbbte3fsPLnTjH6d3Zbbby6MFd3vcJHEKT/QfhFTUdG4ucr4ms7cI7Hn2WXHI8SrU19kTKGHSDlA13RoD8frSrSOEoxuoxt1ZeRA6snm3cxcTcHlXzAtCloJA8wX6ogzMCd8zEAxt06KV99lxzeXfuz89hDKdyttmKWDGTGgkZm9c5lh0gisQqSjHVTfzl73PTim7nN++zDi3bw7EPkJdHb6mibcaseIBid1dBoGblrxvn0bbuPDsuaGLhapCSX+XkWe5/EWVv4g6J+7DCToFVtdTxAIkzxra0/Tq/Sp53ztszvbl7EeBnHWlfI6vhcUt1Q9tldSAQR1G/wCD+NcrOE4NxkrMs001dGQDWLmTjXeTCY+65UElLZUMDlOiqW0+qNNP4SKqsWl9XNbvcpMel9bNbu/aQjE47wxmW1nOsFzAPUACDUEKav0sjVp0E30nZPkTzuZ7V4i742HZc6oSyFVPl/hJmEG73n1qyo9BqC2WuW+HX0mqcbtWv1fh7iYdv+xo2haV1hMRbHlPAjeUJ5TuPrzr3Wpa6utp0ei9JPCy1Z5xefU+K9ePYcmxmCv4U5MRZdCDoSNCRxD7j7VXTptPPI7vDYmliY3pyT+cDcbP7U4xFhMTcmQFRv3nyXB05AST045jWnH+41MRovCzleVJc39vk13vxOj7O2HdxdlG2mxZ5LLbT92qg7swXe8ddJjnO8qTqRTq9xyVXGwwtWSwSSWy7zb6r7vPaSbC4dLahLahVUQFAgAVsJJKyKqdSVSTlN3b3l2sngUAoBQCgFAKAUAoBQHhFAIoLFs4dPuL8CvWvLiedWPAugV5PQoBQCgIztjCXMRjMMbf9nhmL3DMDMw0WOLAaxwDdagnFyqK2xbS0w1WFDC1FP7pq0epbX1butElIqcqym7ZVhDKGHJgCPxrDSe09RlKLvF2LeHwVq3/AGdtE/uqF/IUUUtiPU6s5/fJvrdy/WSMUAoC25EjfOvAx1nh815e0GLjHtA+ZQzfdChmPtWhi3hFL+SClLhqpyfZ8RPTVRrouy43sizbwPiEPdAAGqWx9K9TH1N+ArzDBurJTrKyX2w3Lm+L8FzPbr6icab27XvfVwRsLJ015nhGk6fhVozURUjggEEEESCDIIO4zxFYas7MztIv2n7Y4XAscxD3SP7K3Gb1Y/Z0jrHCrHB6NrYpJrKPF+nHyNWtiYUnxfD34HOsT27x2IbwsOBbD6KioLlwg/YkgyPauhp6JwtFa9V3tvbsuvL3K54yrN6sO5fPY3uz+y+1X/eYjG3LKgSc19yRHEqjZYjqK06uPwEOjSpKX/avVX8CWOGxUs5T1e27Ini7l3Egoq4jFWwxghLtxWI3NHmg/iJ31aRhRpWc9WD7E+rcVqhiru0pPht7zSXMlpgqo6OpE22V5I9CJHvyrcV5K+smuyx6lGbg41EuvZ+PU7P3cbXsthksI8XVzko582rFpGglRmA03bq47S2HrRrOq1k7ZrZe3hsLfR9aEqSgnmiW4m4FUnduHuSAPxNVcVdm83ZGn7T7M/aLOlpXe2wuW80AkpByieDRlMwIM8KjqQTWau1mushrw145LNZrrRlZLGNsZXQPbYCUuKQRIB1B1VoIIPUEcDWcprMk6M45ojHd32aGz8Tj7UyGa1ctk7zbIcCeoIIPOJ41FThqya5LzZDRp6k2uSt3snVTmyUugIggEcjQym1mjFsbLsI2dLNpX+8qKG+QJryoRTukSzxNacdWU21wbdjMr0QigFAKAUAoBQCgFAKAUAoBQCgFAKAUBS7gflQWMPG3rhVhZjPBAJEqp4MdRIHEDX4rzJu2RLTjDWTqbPH1Gxtnfs9pbeYuZZnc6F3YlmaOEsSY4UhHVVj1iK/1qjna2xJcEskuxGdXogFAKAUAoC1iHYKSq5jppIHHmemvtUVaU4wbhHWe5XtftPUEm7SdkUXbbk6PlWOABafUyI9q81IVJPKWquSV79buvA9RlFLNXfh87RYwaJuEk72JljPXfWKWFpU3rJdLi82+302CdSUtuzhuLiru4nn0/WlbBEUPIJ10MRpuO7f8fBrJhkN7y+1jYS2tjDkC/cBOb/w03ZvUnQeh6VcaI0esTNzn9q8X7LeamLxP0lqrb88zjmCwFy9cCrmuXLjRJ1ZiTzP58N9ddOUKMHKTskVGs5yUYrN7juHZns3bwVtAcrXJ87ROurQp3hQQDMaxJ6cTjMbLEzb2Lcvfn/ouKFBUYpb95s7Ns4oE30HhN9No6hxrDXODAgaJqIIJEwF1pNUXaDz48Or1fdznjef3LI2ZtaDKApGg03Dlp7fFQX4kluBiYvYtm8CL9tbhO9mGo/unegHCDz4kkzU8TVpu9NtfN/HtI50ITVpq5zrthsH/AOn3Ld7DO6qTAObzK41ieII58jO+uj0fi/3kZUqyTfmvwUOOwrwklVouy8iV9h9pLi7Ze47PeSFcPlgTmh1UABQw0/k9zUaSw7w89WKtF7LX7nxt6lpga6rw1nm1t+cze7VvEIEQw9w5E6Egkt/KoLdcscaqpM3ZOyyKYFu8o0CugtqI4pmYCf7pY/ymsZJoxlGS7vnia67tD/8AK27AP/8AJcuMP/UtBf8Av+TS/SMN9NdRIa9kgoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAozGd2nGTu9uNDJSqN9pv+URy5zx9N9YDtuH7OvEAndJ1PydaWRnWZcVQBA3Csnl5ntAKAUAoBQCgFAKAUBSeVAUss6fEUMHCe3N43doYkuCMpCKDwVBy5ES1dzouKp4SFt+3t+WOdx026r6yS90+zkz3cS0eQC2nQkSx6QsezNVbp6vK0aK35v0+dRtaMprpVH1e50EhXfTUQPNvlTvC9CQJPQcwRzucY5/Hz6i0ycsjY291REiK6GRQHOe9za6BbWHnzBvFboIZQPUyT/LXRaAw8nKVXda3in4FRpaSlFU1t2mD3P23a/euahPDCnlmLAj3gN81P+oHFU4Q338Lf6I9EwalJ7vU6QuC/fm8zZvIERYH7sSS5B45jln+4K5W2ZdWzuXMfYzoQNG0KEgnKw1VoHI0auhJXRzrsDjnxm1sbimWFS0LSayApfywRoZFsmRzNQUnrTcjWoyc6kpP58zOm1sG0KAUAoBQCgFAKAUAoBQCgFAKAUAoDyaAoV51B04Rx/0oZ2FdDB7QCgFAKAUAoBQCgFAKAUAoDw0AoCIdpewyYq9+0W7nh3dJlcyNlEaiQd2h6Ddvm1welZUKf0pK8euzXzavM0MRgVVlrp2fgRnZG2L2xg9jFYVyjvnW5bMpuAIzEa6Ab4PSrKvh6ek2qlKaulZp7fnga9GpLCJwmtrNkvephhqcPdHpkJ/6hUD/AE9X3TXj7EsdJU29jJZsfab4uyt+1lRHByqwJaQSPNBAXUEQJ9aqcRQWHqOlPNrbw7Pi6jdp1PqR1o7GbW2SRrE9Na1WSkc7R9tcNhQyhhcuif3ancf4n3J149DVjhNGV8RZpWjxfot/lzNWvjKVHJu74HNNm7CxW1r5vPojMS10rC8BCD7UAAATuUa101bF4fR1JU45tLJb+3gVNOlVxM3N5X3nYdj7Jt4SyLVhdBrqdWb7zGN59NOA0iuPxOIqYio6k3n5F3SpRpQ1YmwqAlLd+6FUliAOZ/WtYbsYbsaHsds42kdiIzsco1BClncAg/SZdtPQ8aioxsvnzeQ0IWTfze/UkVTE4oBQCgFAKAUAoBQCgFAKAUAoBQCgKSeFAUAxOhj/AH4b5/0oZLmahgCgPaAUAoBQCgFAKAUAoBQCgFAKAUBbxGHW4pV1DKdCGEg+xr1GTi9aLszEoqSs1dEK2r3YYW4SbTNaJ4DzLPOCQ3tmirejpzEQVp2l4P28DRno6nJ3i2vnMsYPsptPDAWsNjrfgg/Q6CBJkwCjNvkxn416q4/BV251aT1uKf5XkZp4etTWrGSt1fkz8TsfGXFjE4q8BrP7OihCJ0Hli4ZG8EEVDDE4em706af/AFN39jM6NaatKbXUvjLNjsNhhGRLhylTmdQSYIJGW4Qu4ROTcT7e56WxEtrS25L3WfieKej6UNiv1/PQleDQKCBMzJU8OUDgI0000qqm28zeirFzFMwXy75GYgZiF4wu8nSPfcd1RsyWUKuzKLhZlKhgHylSQGghYg5SDB1gjgaxtMZMxsZbBZbaoSCYZySSAASQZbMUOikag5xXlrceJLdb588zZWrYXcN5kxxJ4mN9eyQuA1kyAaA9oBQCgFAKAUAoBQCgFAKAUAoBQFKzrMdKA8I60BUBQHtAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAsXbGi5dCu6dZHEE9R+MHhXpS233mGitLgIDLqCAQRrIO4+leWrOzFyogfrShk0WPxGe6FRTd8JnW7lUZcjWyxtknTPPhmOMjcCYik81b5kQzburZ8e7/RtbCsTmbQERlgTodCTx9NN9SIkRkE8Kyej1QPmgPaAUAoBQCgFAKAUB//2Q==">    
                        </div>
                        <div class="single-marker-controls-wrapper">
                            <div></div>
                       </div>
                    </div>`;
        }
    }

    /**
     * Boilerplate map initialization code starts below:
     */
// Helper function for getting a random point from a cluster object
    getAllDataPoints(cluster) {
        var dataPoints = [];

        // Iterate through all points which fall into the cluster and store references to them
        cluster.forEachDataPoint(dataPoints.push.bind(dataPoints));

        // Randomly pick an index from [0, dataPoints.length) range
        // Note how we use bitwise OR ("|") operator for that instead of Math.floor
        return dataPoints;
    }
}


@Component({
    template: `
        <div>this is modal :)</div>`
})
export class ModalPage {
    myParam: string;

    constructor(
        public viewCtrl: ViewController,
        params: NavParams
    ) {
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
