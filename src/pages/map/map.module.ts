import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {MapPage} from './map';

import {TranslateModule} from '@ngx-translate/core';
import {FirebaseLoginProvider} from "../../providers/firebase-login/firebase-login";
import {FireEventService} from "../../providers/fire.event.service";
import {MapClustering, ModalPage} from "./clustering.helper";
import {EventMapComponent} from "../../components/event-map/event-map";

@NgModule({
    declarations: [
        MapPage,
        EventMapComponent
    ],
    entryComponents: [
    ],
    imports: [
        IonicPageModule.forChild(MapPage),
        TranslateModule.forChild()
    ],
    providers: [
        FirebaseLoginProvider,
        FireEventService,
        MapClustering
    ]
})
export class MePageModule {
}
