import {ChangeDetectorRef, Component, ViewContainerRef} from '@angular/core';
import {App, IonicPage, Nav, NavController, Platform} from 'ionic-angular';

import {MapService} from "./map.service";
import {FireStoreProvider} from "../../providers/fire-store/fire-store";
import {Diagnostic} from '@ionic-native/diagnostic';
import {LocationAccuracy} from '@ionic-native/location-accuracy';
import {HereMap} from "../../providers/here-map-service";
import {DebugService} from "../../providers/debug.service";
import {BackgroundGeolocationConfig, BackgroundGeolocationResponse} from "@ionic-native/background-geolocation";
import {SingleEventPage} from "../single-event/single-event";
import {MapClustering} from "./clustering.helper";

@IonicPage()
@Component({
    selector: 'me',
    templateUrl: 'map.html'
})

export class MapPage {
    private isNoCoords: boolean;
    private users: Object;
    private isLoading: boolean = true;

    constructor(public mapService: MapService,
                public fbs: FireStoreProvider,
                private diagnostic: Diagnostic,
                private locationAccuracy: LocationAccuracy,
                private hereMap: HereMap,
                public nav: Nav,
                private debugService: DebugService,
                private navCtrl: NavController,
                public platform: Platform,
                public app: App,
                private mapClustering: MapClustering,
                private ref: ChangeDetectorRef,
                private currRef: ViewContainerRef
    ) {
        this.isNoCoords = false;
        this.isLoading = true;
        this.fbs.getUsers().subscribe((users: any) => {
            this.setUsers(users);
        });
        this.users = {};
    }

    isDebugEnabled() {
        return this.debugService.isDebug;
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad');



        this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            if (canRequest) {
                // the accuracy option will be ignored by iOS
                this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                    (accuracy: number) => console.log('accuracy yes', accuracy),
                    error => console.log('accuracy NO')
                );
            }
        });

        if (this.debugService.isBrowser){
            /*console.log('asdfsadf');
            this.isLoading = false;
            let location = {lat: 54.7127104,  lng: 25.2895082};
            this.mapService.map = this.hereMap.loadHereMap(location, 'map');
            this.initFireBaseEventSubscriber();*/
        } else {
            this.diagnostic.isLocationEnabled().then((isLocationEnabled: boolean) => {
                if (isLocationEnabled) {
                    this.diagnostic.isLocationAvailable().then((isAbleToShowLocation) => {
                        if (isAbleToShowLocation) {
                            this.initGeoTracking().then((response: boolean) => {
                                this.isLoading = false;
                                this.isNoCoords = false;
                            }).catch((response: boolean) => {
                                this.isLoading = false;
                                this.isNoCoords = true;
                            });
                        }
                    }).catch(() => {
                        console.log('problems with is location');
                    });
                } else {
                    this.isLoading = false;
                    this.isNoCoords = true;
                }
            }).catch(() => {
                console.log('problems with location');
            });
        }

    }

    setUsers(users: any) {
        if (users) {
            users.forEach((user: any) => {
                if (user.uid && user.photoURL &&
                    user.location && user.location.lat && user.location.lng &&
                    this.mapService && this.mapService.fireBaseLogin && this.mapService.fireBaseLogin.user &&
                    user.uid !== this.mapService.fireBaseLogin.user.uid) {
                    this.users[user.uid] = user;
                }
            });
        }
    }

    retry() {
        this.initGeoTracking();
    }

    getIsNoCoords() {
        return this.isNoCoords;
    }

    setIsNoCoords(value: boolean) {
        this.isNoCoords = value;
    }

    refreshCoords() {
        this.showUser(this.fbs.getUID());
    }

    getMeCoords() {
        return this.mapService && this.mapService.getCurrentLocation() || {};
    }

    generateArray(obj) {
        return Object.keys(obj).map((key) => {
            return obj[key]
        });
    }

    showUser(uid: string) {
        let position = this.mapService.markerCollection[uid].getPosition();
        this.mapService.map.setCenter({lng: position.lng, lat: position.lat});
        this.mapService.map.setZoom(18);
    }

    openCreateEvent() {
        this.nav.push('CreateEventPage');
    }

    initGeoTracking() {
        const configForForeGround: BackgroundGeolocationConfig = {
            desiredAccuracy: 10,
            stationaryRadius: 30,
            distanceFilter: 30
        };

        let config: BackgroundGeolocationConfig = {
            desiredAccuracy: 10,
            stationaryRadius: 25,
            distanceFilter: 25,
            debug: false, //  enable this hear sounds for background-geolocation life-cycle.
            stopOnTerminate: true, // enable this to clear background location settings when the app terminates
            fastestInterval: 30000,
            interval: 30000,
            stopOnStillActivity: true,
            activitiesInterval: 5000,
            locationProvider: 1,
            saveBatteryOnBackground: true,
            maxLocations: 1,
            startForeground: false,
            pauseLocationUpdates: true
        };

        if (this.debugService.isDebug) {
            config = this.debugService.getDebugMapConfig();
        }

        this.platform.pause.subscribe(() => {
            this.mapService.backgroundGeolocation.switchMode(0);
            this.mapService.backgroundGeolocation.setConfig(configForForeGround);
            console.log("app going to foreground");
        });

        this.platform.resume.subscribe(() => {
            this.mapService.backgroundGeolocation.setConfig(config);

            console.log("app going to active");
            this.mapService.backgroundGeolocation.start();
        });

        return new Promise((resolve, reject) => {
            this.mapService.locationService = this.mapService.backgroundGeolocation.configure(config);
            this.mapService.locationService.subscribe((location: BackgroundGeolocationResponse) => {
                this.setIsNoCoords(false);
                this.mapService.coords = {lng: location.longitude, lat: location.latitude};
                this.mapService.location = location;
                if (!this.mapService.map) {
                    this.mapService.map = this.hereMap.loadHereMap(this.mapService.coords, 'map');
                    this.initFireBaseUserSubscriber();
                    this.initFireBaseEventSubscriber();
                    //this.setTrackingPosition(location);
                    //this.startWatchingLocation(mePage);
                    resolve(location);
                } else {
                    if (this.fbs.getUID()) {
                        this.mapService.setTrackingPosition(location);
                    }
                }
            });

            this.mapService.backgroundGeolocation.start();
            resolve(true);
        }).catch((reason) => {
            alert(reason);
        });

        // If you wish to turn OFF background-tracking, call the #stop method.
        //this.backgroundGeolocation.stop();
    }

    //TODO refactor and merge all init..subscriber methods
    initFireBaseUserSubscriber() {
        this.fbs.getUsers().subscribe((users: any) => {
            /*users.forEach((user) => {
                if (user && user.uid && user.location && user.location.lat && user.location.lng) {
                    let location = {lng: user.location.lng, lat: user.location.lat};
                    let myUID = this.fbs.getUID();
                    let myEmail = this.fbs.afAuth.auth.currentUser.email;
                    let usrMarker = this.mapService.markerCollection[user.uid];
                    let domIconOptions = {
                        html: `<img class="user-marker" src="${user.photoURL || ''}" width="50" height="50">`
                    };
                    let icon = this.hereMap.domIcon(domIconOptions);

                    if (!usrMarker) {
                        let H = this.hereMap.H;
                        this.mapService.markerCollection[user.uid] = new H.map.DomMarker(location, {
                            icon: icon
                        });
                        this.mapService.map.addObject(this.mapService.markerCollection[user.uid]);
                    } else if (usrMarker) {
                        usrMarker.setPosition(location);
                        usrMarker.setIcon(icon);
                    }
                }
            });*/
        }, err => {
            console.error('Issue while retrieving firebase data');
        })
    }

    //TODO refactor and merge all init..subscriber methods
    initFireBaseEventSubscriber() {
        this.mapService.fsEvents.getAllEventSubscriber(15).subscribe((events) => {
            this.mapClustering.startClustering(this.mapService.map, this.mapService.map.ui, this.mapClustering.getBubbleContent, events);
            this.ref.markForCheck();


            /*if (events && events.length > 0) {
                events.forEach((event: any) => {
                    let eventId = Math.random() * 1000;
                    let eventData = event.event;
                    eventData.location = event.location;
                    if (eventId && eventData.location && eventData.location.lat && eventData.location.lng) {
                        let marker = this.mapService.markerCollection[eventId];
                        let domIconOptions = {
                            html: this.getEventMarker(eventData.img),
                            methodToCall: () => this.openEvent(event)
                        };
                        let icon = this.hereMap.domIcon(domIconOptions);
                        let location = {lng: eventData.location.lng, lat: eventData.location.lat};
                        if (!marker) {
                            let H = this.hereMap.H;
                            marker = new H.map.DomMarker(location, {
                                icon: icon
                            });
                            this.mapService.map.addObject(marker);
                        } else {
                            marker.setPosition(location);
                            marker.setIcon(icon);
                        }
                    }
                })
            }*/
        }, err => {
            console.error('Issue while retrieving firebase data');
        });
    }

    getEventMarker(photo){
        return `<div class="event-marker-wrapper">
                    <img src="${photo}" width="50" height="50" class="event-marker">
                </div>`;
    }

    openEvent(event: any) {
        this.app.getRootNav().push(SingleEventPage, {'selectedEvent': event});
    }
}
