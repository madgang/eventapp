import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

import { ErrorsProvider } from '../../providers/errors/errors';
import { FirebaseFirestoreProvider } from '../../providers/firebase-firestore/firebase-firestore';
import { ToastProvider } from '../../providers/toast/toast';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  private loading: boolean[] = [];
  private language: string;

  constructor(
    private errorsProvider: ErrorsProvider,
    private firebaseFirestoreProvider: FirebaseFirestoreProvider,
    private toastProvider: ToastProvider,
    private translateService: TranslateService
  ) {
    this.language = this.translateService.currentLang;
  }

  languageChange(language: string): void {
    this.loading.push(true);

    this.translateService.use(language);

    this.firebaseFirestoreProvider.setUser(
      '',
      {
        settings: {
          language: language
        }
      }
    )
      .then(() => {
        this.loading.pop();
      })
      .catch((error: any) => {
        this.loading.pop();
        this.toastProvider.presentErrorToast('SET_LANGUAGE_ERROR');
        this.errorsProvider.setUserError(error, 'SettingsPage.languageChange().setUser()');
      });
  }

}
