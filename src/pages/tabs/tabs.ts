import {Component, ViewChild} from '@angular/core';
import {
    IonicPage,
    MenuController, Tabs
} from 'ionic-angular';
import {TabService} from "../../providers/tab.service";

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
    @ViewChild('tabs') tabRef: Tabs;

  constructor(
    private menuCtrl: MenuController, private tabService: TabService
  ) {
  }

    ionViewDidLoad() {
        this.tabService.tabRef = this.tabRef;
    }

  openSideMenu(): void {
    this.menuCtrl.open('right');
  }

}
