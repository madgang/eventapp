import {Component} from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController} from 'ionic-angular';

import {FacebookLoginResponse} from '@ionic-native/facebook';
import {TranslateService} from '@ngx-translate/core';

import {ErrorsProvider} from '../../providers/errors/errors';
import {FacebookProvider} from '../../providers/facebook/facebook';
import firebase from 'firebase/app';
import {FirebaseFirestoreProvider} from '../../providers/firebase-firestore/firebase-firestore'; //INFO: implementation 1
import {FirebaseLoginProvider} from '../../providers/firebase-login/firebase-login';
import {FireStoreProvider} from '../../providers/fire-store/fire-store'; //INFO: implementation 2
import {GoogleProvider} from '../../providers/google/google';
import {ToastProvider} from '../../providers/toast/toast';
import {Diagnostic} from "@ionic-native/diagnostic";

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    private defaultLang: string = 'en' //EDIT: default language
    private loading: Loading;

    constructor(
        private errorsProvider: ErrorsProvider,
        private facebookProvider: FacebookProvider,
        private firebaseFirestoreProvider: FirebaseFirestoreProvider,
        private firebaseLoginProvider: FirebaseLoginProvider,
        private fireStoreProvider: FireStoreProvider,
        private googleProvider: GoogleProvider,
        private loadingCtrl: LoadingController,
        private navCtrl: NavController,
        private toastProvider: ToastProvider,
        private translateService: TranslateService,
        private diagnostic: Diagnostic
    ) {


    }

    //TODO complete this:  request accesses at application start
    ionViewDidLoad() {
        this.diagnostic.requestRuntimePermissions([
            this.diagnostic.permission.ACCESS_FINE_LOCATION
        ]).then(statuses => {
            console.log(statuses)
        }).catch(err => console.error(err))
    }

    firebaseSignInWithFacebook(): void {
        this.presentLoading();
        this.facebookProvider.login()
            .then((loginResponse: FacebookLoginResponse) => {
                this.firebaseLoginProvider.signInWithFacebook(loginResponse.authResponse.accessToken)
                    .then((user: firebase.User) => {
                        this.signInSuccess(user);
                    })
                    .catch((error: any) => {
                        this.loading.dismiss()
                            .then(() => {
                                this.toastProvider.presentErrorToast(error.code);
                                this.errorsProvider.setError(error, 'LoginPage.firebaseSignInWithFacebook().login().signInWithFacebook()'); //EDIT: error info
                            });
                    });
            })
            .catch((error: any) => {
                this.loading.dismiss()
                    .then(() => {
                        this.toastProvider.presentErrorToast('FACEBOOK_LOGIN_ERROR');
                        this.errorsProvider.setError(error, 'LoginPage.firebaseSignInWithFacebook().login()'); //EDIT: error info
                    });
            });
    }

    firebaseSignInWithGoogle(): void {
        this.presentLoading();
        this.googleProvider.login()
            .then((response: any) => {
                this.firebaseLoginProvider.signInWithGoogle(response.idToken)
                    .then((user: firebase.User) => {
                        this.signInSuccess(user);
                    })
                    .catch((error: any) => {
                        this.loading.dismiss()
                            .then(() => {
                                this.toastProvider.presentErrorToast(error.code);
                                this.errorsProvider.setError(error, 'LoginPage.firebaseSignInWithGoogle().login().signInWithGoogle()'); //EDIT: error info
                            });
                    });
            })
            .catch((error: any) => {
                this.loading.dismiss()
                    .then(() => {
                        this.toastProvider.presentErrorToast('GOOGLE_LOGIN_ERROR');
                        this.errorsProvider.setError(error, 'LoginPage.firebaseSignInWithGoogle().login()'); //EDIT: error info
                    });
            });
    }

    private presentLoading(): void {
        this.loading = this.loadingCtrl.create();
        this.loading.present();
    }

    private signInSuccess(user: firebase.User): void {
        this.firebaseFirestoreProvider.getUserDoc('')
            .then((documentSnapshot: firebase.firestore.DocumentSnapshot) => {

                let language: string;

                if (documentSnapshot.exists) {
                    language = documentSnapshot.get('settings.language');
                } else {
                    language = this.defaultLang;
                }

                console.log('signin success');
                this.translateService.use(language);

                this.facebookProvider.getMyFriends().then((friends) => {
                    let fbFriends = {};
                    if (friends.data.length > 0) {
                        friends.data.forEach((friend) => {
                            fbFriends[friend.id] = true;
                        });
                    }
                    this.firebaseFirestoreProvider.setUserCustomData({
                        data: {fbFriends: fbFriends}
                    });
                }).catch((err) => {
                    console.error('Failed to fetch fb friends', err);
                });

                console.log(user.providerData[0], 'provider');
                this.firebaseFirestoreProvider.setUserData(user, language);

                //INFO: implementation 2
                //this.fireStoreProvider.updateFirebaseWithData(); //TODO: use promise

            })
            .catch((error: any) => {
                //TODO: add proper error handling
                this.errorsProvider.setUserError(error, 'LoginPage.signInSuccess().getUserDoc()'); //EDIT: error info
            });

        this.loading.dismiss()
            .then(() => {
                this.navCtrl.setRoot('TabsPage'); //EDIT: homepage
            });
    }

}
