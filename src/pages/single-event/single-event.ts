import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FirebaseLoginProvider } from "./firebase-login/firebase-login";
import { FireEventService } from "../../providers/fire.event.service";
import { ToastProvider } from '../../providers/toast/toast';

import { FirebaseFirestoreProvider } from '../../providers/firebase-firestore/firebase-firestore';

/**
 * Generated class for the SingleEventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-single-event',
    templateUrl: 'single-event.html',
})
export class SingleEventPage {
    private event: any;
    isClicked: boolean;

    constructor(
        params: NavParams,
        public eventService: FireEventService,
        private toastProvider: ToastProvider,
        private firebaseFirestoreProvider: FirebaseFirestoreProvider
    ) {
        this.event = params.get('selectedEvent');
        var inside = false;

        this.event.participants.forEach(user => {
            if (user.data().participant.id === this.eventService.getUid()) {
                inside = true;
            }
        });

        if(inside){
            this.isClicked = false;
        }
        else{
            this.isClicked = true;
        }
    }

    getImage() {
        return this.event.data().event.img;
    }

    addParticipant() {
        if (this.isClicked) {
            this.isClicked = false;
            var inside = false;
            this.event.participants.forEach(user => {
                if (user.data().participant.id === this.eventService.getUid()) {
                    this.toastProvider.presentInfoToast('You Already In');
                    inside = true;
                    return;
                }
            });
            console.log(this.eventService.getUid());
            if (!inside)
                this.eventService.addParticipant(this.event.data().eid);
                this.firebaseFirestoreProvider.setUser(
                    '/goingEvents/' + this.event.data().eid,
                    {
                        startTime: this.event.data().event.time.startTime
                    }
                )
                    .then(() => {
                        //TODO: do something
                    })
                    .catch((error: any) => {
                        //TODO: Add error handling
                    });
        }
    }

    getParticipantsCount(event) {
        return this.event.participantsCount || '0';
    }
}
