import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Page2 } from './page2';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    Page2
  ],
  imports: [
    IonicPageModule.forChild(Page2),
    TranslateModule.forChild()
  ]
})
export class Page2Module {}
