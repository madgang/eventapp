import { Component } from '@angular/core';
import {
  IonicPage,
  MenuController,
  NavController
} from 'ionic-angular';

import { Event } from '../../app/event';

import { EventService } from '../../providers/event-service';
import {FireStoreProvider} from "../../providers/fire-store/fire-store";

@IonicPage()
@Component({
  selector: 'page-mock-events',
  templateUrl: 'mock-events.html'
})
export class MockEventsPage {

  events: Event[];
  id: number;
  showEvents: number;

  constructor(
    private eventService: EventService,
    private menuCtrl: MenuController,
    private navCtrl: NavController,
    private fireStoreProvider: FireStoreProvider,
  ) {
    //  this.fireStoreProvider.updateFirebaseWithData();
    menuCtrl.enable(true);

  }

  ionViewDidLoad() {
    this.showEvents = 0;
    this.eventService.getEvents()
      .then(events => this.events = events.slice(0, this.showEvents = this.showEvents + 5));
  }

  doInfinite(infiniteScroll) {
    setTimeout(() => {
      this.eventService.getEvents()
        .then(events => this.events = events.slice(0, this.showEvents = this.showEvents + 5));

      infiniteScroll.complete();
    }, 500);
  }

  goToEventPage(id: number) {
    this.id = id;
    this.navCtrl.push('EventPage', {id: this.id});
  }

  goToCreateEventPage() {
    this.navCtrl.push('CreateEventPage');
  }

  goToInviteFriendsPage() {
    this.navCtrl.push('InviteFriendsPage');
  }

}
