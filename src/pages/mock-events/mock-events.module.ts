import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MockEventsPage } from './mock-events';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MockEventsPage
  ],
  imports: [
    IonicPageModule.forChild(MockEventsPage),
    TranslateModule.forChild()
  ]
})
export class MockEventsPageModule {}
