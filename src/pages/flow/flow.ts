import {AfterContentInit, Component, ElementRef, ViewChild} from '@angular/core';
import {IonicPage, MenuController, Nav} from 'ionic-angular';

import {Event} from '../../app/event';
import {FbEventsService} from "../../providers/fb-events-service";

import {AngularFireAuth} from 'angularfire2/auth';
import {Observable} from 'rxjs/Observable';

import * as firebase from 'firebase/app';

import {AngularFireDatabase} from 'angularfire2/database';
import {UserService} from "../../providers/userService";
import {FireStoreProvider} from "../../providers/fire-store/fire-store";


interface Users {
    displayName: string;
    email: string;
    location:string;
    photoURL:string;
}

@IonicPage()
@Component({
    selector: 'page-flow',
    templateUrl: 'flow.html'
})

export class FlowPage implements AfterContentInit {
    @ViewChild(Nav) nav: Nav;
    @ViewChild('fb_button') el: ElementRef;

    events: Event[];
    id: number;
    scrollMore: null;
    items: Observable<any[]>;
    avatar: string;
    users:Array<Users>;

    constructor(public navCtrl: Nav,
                public fbEvents: FbEventsService,
                public menuCtrl: MenuController,
                auth: AngularFireAuth,
                afd: AngularFireDatabase,
                user: UserService,
                public fbs: FireStoreProvider) {

        menuCtrl.enable(true);
        this.events = [];
        this.getLocation();
    }

    getLocation()
    {
        this.items=this.fbs.getUsers();
        this.items.subscribe(user=>{
        this.users=user;
        }
        );
        console.log(this.users);
    }

    getAvatarUrl() {
        return this.avatar;
    }

    getEvents() {
        return this.events;
    }

    ngAfterContentInit(): void {
        this.fbEvents.getFbService().getLoginStatus().then(
            (response: any) => this.initContent(response)
        );
    }

    initContent(response) {
        if (response.status == "connected") {
            this.removeFbButton();
            this.fbEvents.getEventByQuery("vilnius").then(
                (response: any) => this.populateEvents(response),
                (error: any) => console.error(error));
        }
    }

    populateEvents(fbQueryResponse) {
        this.loadEvents(fbQueryResponse.data, 0, fbQueryResponse);
    }

    loadEvents(array, index, fbQueryResponse) {
        if (!index) {
            index = 0;
        }
        if (!(array.length > 0)) {
            this.scrollMore = null;
            return;
        }
        var isLast = (array.length - 1) === index;
        var _scope = this;
        var event = array[index];
        this.fbEvents.getEventById(event.id).then((eventData) => {
            event.img = eventData.cover && eventData.cover.source;
            _scope.events.push(event);
            if (isLast) {
                _scope.finalEventAssignment(fbQueryResponse);
            } else {
                this.loadEvents(array, (index + 1), fbQueryResponse);
            }
        });
    }

    finalEventAssignment(fbQueryResponse) {
        this.scrollMore =   fbQueryResponse.paging && fbQueryResponse.paging.next || null;
    }

    getImgUrl(event) {
        return event.img;
    }

    goToEventPage(id: number) {
        this.id = id;
        this.navCtrl.push('EventPage', {id: this.id});
    }

    goToCreateEventPage() {
        this.navCtrl.push('CreateEventPage');
    }

    removeFbButton() {
        new Promise((resolve) => {
            resolve(this.el);
        }).then((element: any) => {
            element.getNativeElement().hidden = true;
        });
    }

    doInfinite(infiniteScroll) {
        if (!this.scrollMore) {
            infiniteScroll.complete();
            return true;
        }
        this.fbEvents.setParsedEventList(this.events, infiniteScroll, this.scrollMore).subscribe((data) => {
            this.populateEvents(data);
            infiniteScroll.complete();
        });
    }

    loginFB() {
        this.fbEvents.getFbService().login(['public_profile', 'user_friends', 'email', 'user_events']).then(
            (response: any) => this.removeFbButton(),
            (error: any) => console.error(error)
        );
    }

    getImgageEvent(event) {
        var imgLink = event && event.additionalDataAsync || null;
        if (imgLink) {
            imgLink.then((img) => {
                return img;
            });
        }
        return 'na';
    }

    getEventDescription(event) {
        if (event && event.description) {
            return `${event.description.slice(0, 200)} ${event.description.length > 200 ? '...' : ''}`
        } else {
            return '';
        }
    }

}
