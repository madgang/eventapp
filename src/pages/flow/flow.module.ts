import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FlowPage } from './flow';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FlowPage
  ],
  imports: [
    IonicPageModule.forChild(FlowPage),
    TranslateModule.forChild()
  ]
})
export class FlowPageModule {}
