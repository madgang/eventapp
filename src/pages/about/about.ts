import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import {FirebaseLoginProvider} from "../../providers/firebase-login/firebase-login";

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
    private user:any;

  constructor(private firebaseLoginProvider: FirebaseLoginProvider) { }

  getUserName(){
      return this.firebaseLoginProvider.user.displayName;
  }

}
