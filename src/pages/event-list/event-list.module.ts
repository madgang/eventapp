import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventListPage } from './event-list';

import { TranslateModule } from '@ngx-translate/core';
import {SingleEventPageModule} from "../single-event/single-event.module";
import {SingleEventPage} from "../single-event/single-event";

@NgModule({
  declarations: [
    EventListPage,

  ],
  imports: [
    IonicPageModule.forChild(EventListPage),
    TranslateModule.forChild(),
  ],
})
export class EventListPageModule {}
