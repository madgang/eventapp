import { Component } from '@angular/core';
import {App, IonicPage, NavController} from 'ionic-angular';
import {SingleEventPage} from "../single-event/single-event";
import {FireEventService} from "../../providers/fire.event.service";
import * as moment from 'moment';
import * as firebase from "firebase";

@IonicPage()
@Component({
  selector: 'page-event-list',
  templateUrl: 'event-list.html',
})
export class EventListPage {
    private events: any = [];
    number: number;
    mom: any;

    constructor(public navCtrl: NavController, public app: App,
                private eventService: FireEventService) {
        moment.locale('en-gb');
        this.mom=moment();

       // console.log(this.mom.isoWeekday());
       // console.log(moment().endOf('day').fromNow());
      //  console.log(moment().add(1000.6, 'hour').calendar());

        eventService.getAllFriendEvents(6).then((snap)=>{
            this.events = snap.docs;
            this.showParticipants(this.events);
            this.getParticipants(this.events);
        });
    }

    showParticipants(events)
    {
        events.forEach((event: firebase.firestore.DocumentReference) =>{
            this.eventService.getParticipantsLenght(event);
        });
    }
    getParticipants(events)
    {
        events.forEach((event: firebase.firestore.DocumentReference) =>{
            this.eventService.getParticipants(event);
        });
    }

    openEventPage(event:any) {
        this.app.getRootNav().push(SingleEventPage, { 'selectedEvent': event });
    }

    checkAvailability(eventData) {
        //  var currentDate = new Date().toISOString();
        // var update = function(){
        //     this.mom=moment();
        // };
        // setInterval(update, 60000);

        var us = moment(eventData).format('YYYY-MM-DD HH:mm:ss');
        var duration = moment.duration(moment().diff(us));
        this.number = duration.asMinutes();

        if(this.number<0)
            this.number= this.number * -1;

            return moment().add(this.number, 'minute').calendar();

    }

    doInfinite(infiniteScroll) {
        let loadMore = this.eventService.loadMoreEvents();
        if(loadMore) {
            loadMore.then((snap)=>{
                this.events = [...this.events, ...snap.docs];
                this.showParticipants(this.events);
                this.getParticipants(this.events);

                infiniteScroll.complete();
            });
        } else {
            infiniteScroll.complete();
        }
    }

    doRefresh (refresh){
        //refresh.complete();
        this.eventService.getAllFriendEvents(6).then((snap)=>{
            this.events = snap.docs;
            this.showParticipants(this.events);
            this.getParticipants(this.events);
            refresh.complete();
        }).catch((err)=>{
            refresh.complete();
            console.log(err);
        });
    }

    getParticipantsCount(event) {
        return event.participantsCount || '';
    }
}
