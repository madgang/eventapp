import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavsPage } from './favs';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FavsPage
  ],
  imports: [
    IonicPageModule.forChild(FavsPage),
    TranslateModule.forChild()
  ]
})
export class FavsPageModule {}
