import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import firebase from 'firebase/app';

import { ErrorsProvider } from '../../providers/errors/errors';
import { FirebaseFirestoreProvider } from '../../providers/firebase-firestore/firebase-firestore';
import { ToastProvider } from '../../providers/toast/toast';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  private loading: boolean[] = [];
  private user: any;

  constructor(
    errorsProvider: ErrorsProvider,
    firebaseFirestoreProvider: FirebaseFirestoreProvider,
    toastProvider: ToastProvider
  ) {
    this.loading.push(true);

    firebaseFirestoreProvider.getUserDoc('')
      .then((documentSnapshot: firebase.firestore.DocumentSnapshot) => {
        this.user = documentSnapshot.get('user');
        this.loading.pop();
      })
      .catch((error: any) => {
        this.loading.pop();
        toastProvider.presentErrorToast('GET_USER_ERROR');
        errorsProvider.setUserError(error, 'ProfilePage.getUserDoc()');
      });
  }

}
