import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InviteFriendsPage } from './invite-friends';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    InviteFriendsPage
  ],
  imports: [
    IonicPageModule.forChild(InviteFriendsPage),
    TranslateModule.forChild()
  ]
})
export class InviteFriendsPageModule {}
