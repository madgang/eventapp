import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderReverseResult} from "@ionic-native/native-geocoder";
import {MapService} from "../map/map.service";
import {CreateEventService} from "../../providers/create-event-service";
import {ILatLng} from "@ionic-native/google-maps";
import {HereMap} from "../../providers/here-map-service";

/**
 * Generated class for the SetLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


declare var google: any;

@IonicPage()
@Component({
    selector: 'page-set-location',
    templateUrl: 'set-location.html',
})
export class SetLocationPage {
    autoCompleteService = new google.maps.places.AutocompleteService();
    autoCompleteItems;
    autoComplete;
    map;
    marker;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private nativeGeocoder: NativeGeocoder,
                private mapService: MapService,
                private hereMap: HereMap,
                private createEventService: CreateEventService) {
        this.autoCompleteItems = [];
        this.autoComplete = {
            query: ''
        };
    }

    ionViewDidLoad() {
        if (this.createEventService.map) {
            this.autoComplete.query = this.createEventService.query;
            this.initMap(this.createEventService.location, this.createEventService.query);
        } else if (this.mapService.getCurrentLocation()) {
            this.createEventService.query = 'Current location';
            this.createEventService.setLocationValue = this.createEventService.query;
            const location = this.mapService.getCurrentLocation();
            const locationLatLng = {
                lat: location && (location.latitude || location.lat),
                lng: location && (location.longitude || location.lng)
            };
            this.createEventService.location = locationLatLng;
            this.initMap(locationLatLng, this.createEventService.query);
        }
    }

    onInputAutoComplete(event: any) {
        if (!this.autoComplete.query) {
            return;
        }
        this.autoCompleteService.getPlacePredictions({
            input: this.autoComplete.query,
            componentRestrictions: {country: 'LT'}
        }, (predictions, status) => {
            console.log(predictions);
            this.autoCompleteItems = [];
            /*predictions.forEach((item) => {
                this.autoCompleteItems.push(item.description);
            })*/
            this.autoCompleteItems = predictions;
        });
    }

    onCancelAutoComplete(event: any) {

    }

    testGeoCoder() {
        let locationToTest = 'Berlin';
        this.nativeGeocoder.forwardGeocode(locationToTest)
            .then((coordinates: NativeGeocoderForwardResult) => {
                coordinates = coordinates[0];
                console.log('The coordinates are latitude=' + coordinates.latitude + ' and longitude=' + coordinates.longitude);
                this.nativeGeocoder.reverseGeocode(parseFloat(coordinates.latitude), parseFloat(coordinates.longitude))
                    .then((result: NativeGeocoderReverseResult) => console.log(JSON.stringify(result)))
                    .catch((error: any) => console.log(error));
            }).catch((error: any) => console.log(error));
    }

    chooseItem(item) {
        this.autoComplete.query = item.description;
        this.autoCompleteItems = [];

        this.createEventService.query = item.description;
        this.createEventService.setLocationValue = item.description;

        let location = null;

        let geocoder = new google.maps.Geocoder();
        geocoder.geocode({'address': item.description}, (results, status) => {
            let latitude = results[0].geometry.location.lat();
            let longitude = results[0].geometry.location.lng();
            this.createEventService.location = {lat: latitude, lng: longitude};
            if (!this.map) {
                this.initMap({lng: longitude, lat: latitude}, item.description);
            } else {
                this.updateMap({lng: longitude, lat: latitude}, item.description);
            }
        });
    }

    getAutoCompleteItems() {
        return this.autoCompleteItems;
    }

    updateMap(location: ILatLng, description: String) {
        this.updateMarker(location, description);
        this.map.setCenter(location);
        this.map.setZoom(16);
    }

    updateMarker(location: ILatLng, description: String): void {
        this.marker.setPosition(location);
    }

    initMap(location: any, description: String) {
        this.createEventService.location = location;
        this.map = this.hereMap.loadHereMap(location, 'evCrMap');
        let domIconOptions = {
            html: `<img class="marker-with-shadow" src="./assets/img/marker.png" width="50" height="50">`
        };
        this.marker = new this.hereMap.H.map.DomMarker(location, {
            icon: this.hereMap.domIcon(domIconOptions)
        })
        ;
        //this.marker.setIcon();

        this.map.addObject(this.marker);
        this.createEventService.map = this.map;
    }
}
