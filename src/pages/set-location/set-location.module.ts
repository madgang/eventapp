import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SetLocationPage} from './set-location';
import {NativeGeocoder} from "@ionic-native/native-geocoder";

@NgModule({
    declarations: [
        SetLocationPage,
    ],
    imports: [
        IonicPageModule.forChild(SetLocationPage),
    ],
    providers: [
        NativeGeocoder
    ]
})
export class SetLocationPageModule {
}
