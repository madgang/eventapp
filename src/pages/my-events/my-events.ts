import { Component } from '@angular/core';
import {
  IonicPage,
  NavController
} from 'ionic-angular';

import firebase from 'firebase/app';

import { ErrorsProvider } from '../../providers/errors/errors';
import { FirebaseFirestoreProvider } from '../../providers/firebase-firestore/firebase-firestore';
import { ToastProvider } from '../../providers/toast/toast';

@IonicPage()
@Component({
  selector: 'page-my-events',
  templateUrl: 'my-events.html',
})
export class MyEventsPage {

  private eventsObject: object[] = [];
  private loading: boolean = true;

  constructor(
    private errorsProvider: ErrorsProvider,
    private firebaseFirestoreProvider: FirebaseFirestoreProvider,
    private navCtrl: NavController,
    private toastProvider: ToastProvider
  ) {
    this.LoadList()
      .then(() => {
        this.loading = false;
      });
  }

  LoadList(refreshing?: true): Promise<null> {
    return new Promise((resolve) => {

      this.firebaseFirestoreProvider.getUserCollectionGoingEvents()
        .then((querySnapshot: firebase.firestore.QuerySnapshot) => {

          let eventsArray: any[] = [[]];
          const date = new Date();
          let i: number = 0;
          let oldDate: string = null;

          querySnapshot.forEach((result: firebase.firestore.QueryDocumentSnapshot) => {
            
            date.setTime(result.data().startTime);
            const newDate = date.getFullYear().toString() + date.getMonth().toString() + date.getDate().toString();

            this.firebaseFirestoreProvider.getDoc('/events/' + result.id)
              .then((documentSnapshot: firebase.firestore.DocumentSnapshot) => {

                i++;

                if (eventsArray[0].length != 0) {

                  if (oldDate != newDate) {
                    eventsArray.push([]);
                  }

                  eventsArray[eventsArray.length - 1].push(documentSnapshot.data());

                } else {
                  eventsArray[0].push(documentSnapshot.data());
                }

                oldDate = newDate;

                if (refreshing == true && i == querySnapshot.size) {
                  this.eventsObject = eventsArray;
                  resolve();
                } else if (refreshing != true) {
                  this.eventsObject = eventsArray;
                  resolve();
                }

              })
              .catch((error: any) => {
                this.toastProvider.presentErrorToast('GET_EVENT_DATA_ERROR');
                this.errorsProvider.setUserError(error, 'MyEventsPage.getUserCollection.getDoc()');
                resolve();
              });
          });

          if (querySnapshot.size == 0) {
            resolve();
          }

        })
        .catch((error: any) => {
          this.toastProvider.presentErrorToast('GET_EVENTS_ERROR');
          this.errorsProvider.setUserError(error, 'MyEventsPage.getUserCollection()');
          resolve();
        });
    });
  }

  openSingleEventPage(eid: string): void {
    this.navCtrl.push('SingleEventPage', {'selectedEvent': eid});
  }

  doRefresh(refresher) {
    this.LoadList(true)
      .then(() => {
        refresher.complete();
      });
  }

}
