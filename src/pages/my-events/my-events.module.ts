import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyEventsPage } from './my-events';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MyEventsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyEventsPage),
    TranslateModule.forChild()
  ],
})
export class MyEventsPageModule {}
