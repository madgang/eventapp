import { Component, OnInit} from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

import { Event } from '../../app/event'
import { EventService } from '../../providers/event-service'

@IonicPage()
@Component({
  selector: 'page-event',
  templateUrl: 'event.html'
})
export class EventPage implements OnInit {

  event: Event;
  id: number;

  constructor(
    private eventService: EventService,
    private navParams: NavParams
  ) {

    this.id = navParams.get('id');
  
  }

  ngOnInit(): void {
    this.eventService.getEvent(this.id).then(event => this.event = event);
  }

}
