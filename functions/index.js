// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const rp = require('request-promise');
const promisePool = require('es6-promise-pool');
const PromisePool = promisePool.PromisePool;
const secureCompare = require('secure-compare');
const MAX_CONCURRENT = 3;
const Firestore = require('@google-cloud/firestore');
const firestore = new Firestore();
const gcs = require('@google-cloud/storage')();
const images = require('circle-image');
const imageSizes = [80];
const Jimp = require("jimp");
const mkdirp = require('mkdirp-promise');
const path = require('path');
const os = require('os');
const fs = require('fs');
const JPEG_EXTENSION = '.jpg';
const easyimg = require('easyimage');
const UUID = require("uuid-v4");
const bucket = gcs.bucket('proj1-256a7.appspot.com');

exports.updateUser = functions.firestore
    .document('users/{userId}')
    .onUpdate(event => {
        var newValue = event.data.data();
        var usersDate = newValue.lastSeen;
        var currentDate = new Date();
        var difference = currentDate - usersDate;
        var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
        difference -= daysDifference * 1000 * 60 * 60 * 24
        var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
        difference -= hoursDifference * 1000 * 60 * 60
        var minutesDifference = Math.floor(difference / 1000 / 60);
        difference -= minutesDifference * 1000 * 60
        var secondsDifference = Math.floor(difference / 1000);
        if (minutesDifference < 5) {
            return event.data.ref.set({
                status: "Online"
            }, {merge: true});
        } else {
            return event.data.ref.set({
                status: "Offline"
            }, {merge: true});
        }
        ;
    });

exports.setStatus = functions.https.onRequest((req, res) => {
// Exit if the keys don't match
    const key = req.query.key;
    if (!secureCompare(key, functions.config().cron.key)) {
        console.log('The key provided in the request does not match the key set in the environment. Check that', key,
            'matches the cron.key attribute in `firebase env:get`');
        res.status(403).send('Security key does not match. Make sure your "key" URL query parameter matches the ' +
            'cron.key environment variable.');
        return;
    }
    var inactiveUsers = [];
    var currentDate = new Date();
// Fetch all user details.

    getUsers().then(users => {
        console.log(users);
        var l = users.length;
        console.log(l);
        for (var i = 0; i < l; i++) {
            if (check(users[i])) {
                inactiveUsers.push(users[i]);
                console.log(users[i]);
            }
        }

// Find users that have not signed in in the last 5 min.
// const inactiveUsers = users.filter(check);
//console.log(users.filter(check));
// user => user.lastSeen < currentDate - 5 * 60 * 1000);
        console.log(inactiveUsers.length);
        console.log(inactiveUsers);
// Use a pool so that we delete maximum `MAX_CONCURRENT` users in parallel.
        const promisePool = new PromisePool(() => {
                if (inactiveUsers.length > 0) {
                    var userToDelete = inactiveUsers.pop();
                    console.log(userToDelete.localId);
                    // Delete the inactive user.
                    var data = {
                        status: "Offline"
                    }
                    return firestore.collection('users').doc(userToDelete.localId).update(data);
                }
            },
            MAX_CONCURRENT
            )
        ;


        promisePool.start().then(() => {
            console.log('User status update finished');
            res.send('User status update finished');
        })
        ;
    })
    ;
})
;

//exports.saveToStorage = functions.database.ref('/users/{userId}/photoURL')
exports.saveURL = functions.firestore.document('users/{userId}/{photoURL}')
    .onWrite(event => {
        console.log("INSIDE SAVETOSTORAGE!!!!!");
        const filePath = event.data.photoURL;
        const filename = filePath.split('/').pop();
        const bucket = gcs.bucket('proj1-256a7.appspot.com');
//var localReadStream = fs.createReadStream();
        const remoteWriteStream = bucket.file(filename).createWriteStream({
            metadata: {contentType: 'image/jpeg'}
        });
        request(filePath).pipe(remoteWriteStream)
            .on('error', (err) => console.log(err))
            .on('finish', () => console.log('success save image'));
    });


exports.imageToRound = functions.storage.object().onChange(event => {
    const object = event.data;

    if (object.contentType.startsWith('image/jpg')) {
        const filePathNew = object.name;
        const fileDir = path.dirname(filePathNew);
        const baseFileName2 = path.basename(filePathNew, path.extname(filePathNew));
        console.log('NEW IMAGE ID : ' + baseFileName2.slice(1));
        const oldpath = path.normalize(path.format({dir: fileDir, name: a + baseFileName2, ext: '.png'}));
        gcs.bucket(object.bucket)
            .file(oldpath)
            .delete().then(() => {
            console.log("Old PNG were deleted");
        })
            .catch(err => {
                console.error('ERROR:', err);
            });
    }
// Exit if this is a move or deletion event.
    if (object.resourceState === 'not_exists') {
        return console.log('This is a deletion event.');
    }
// Exit if the image is already a PNG.
    if (object.contentType.startsWith('image/png')) {
        console.log('Already a PNG.');
        const filePathNew = object.name;
        const fileDir = path.dirname(filePathNew);
        const baseFileName2 = path.basename(filePathNew, path.extname(filePathNew));
        console.log('NEW IMAGE ID : ' + baseFileName2.slice(1));
        const oldpath = path.normalize(path.format({dir: fileDir, name: baseFileName2.slice(1), ext: JPEG_EXTENSION}));
        return gcs.bucket(object.bucket)
            .file(oldpath)
            .delete().then(() => {
                console.log("Old JPEG were deleted");
            })
            .catch(err => {
                console.error('ERROR:', err);
            });
    }


    const filePath = object.name;
    const fileName = path.basename(filePath);
    const baseFileName = path.basename(filePath, path.extname(filePath));
    const fileDir = path.dirname(filePath);
    const JPEGFilePath = path.normalize(path.format({dir: fileDir, name: baseFileName, ext: JPEG_EXTENSION}));
    const JPEGFilePath2 = path.normalize(path.format({dir: fileDir, name: 'a' + baseFileName, ext: JPEG_EXTENSION}));
    const JPEGFilePath3 = path.normalize(path.format({dir: fileDir, name: 'a' + baseFileName, ext: '.png'}));
    const tempLocalFile = path.join(os.tmpdir(), filePath);
    const tempLocalDir = path.dirname(tempLocalFile);
    const tempLocalJPEGFile = path.join(os.tmpdir(), JPEGFilePath);
    const tempLocalJPEGFile3 = path.join(os.tmpdir(), JPEGFilePath3);

    console.log(filePath + "OPAAA");

    return mkdirp(tempLocalDir).then(() => {
        // Download file from bucket.
        return bucket.file(filePath).download({destination: tempLocalFile});
    }).then(() => {
        console.log('The file has been downloaded to', tempLocalFile);
        //Resize and Circulize
        return resizeImageToSize(tempLocalFile, 90, tempLocalFile, tempLocalJPEGFile3);
    }).then(() => {
        console.log('Circulized image created at', tempLocalJPEGFile);
// Uploading the Circulized image.
//return bucket.upload(tempLocalJPEGFile3, {destination: JPEGFilePath3}).then(() => {
        return upload(tempLocalJPEGFile3, JPEGFilePath3)
            .then(downloadURL => {
                console.log('JPEG image uploaded to Storage at', JPEGFilePath);
                console.log('Download link ', downloadURL);
                var data = {
                    photoURL: downloadURL
                }
                firestore.collection('users').doc(baseFileName).update(data);
                // Once the image has been converted delete the local files to free up disk space.
                fs.unlinkSync(tempLocalJPEGFile3);
            });

//fs.unlinkSync(tempLocalFile);
    });
});


function check(user) {
    var currentDate = new Date();

    var difference = currentDate - user.lastLoginAt;

    var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
    difference -= daysDifference * 1000 * 60 * 60 * 24;

    var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
    difference -= hoursDifference * 1000 * 60 * 60;

    var minutesDifference = Math.floor(difference / 1000 / 60);
    difference -= minutesDifference * 1000 * 60;

    var secondsDifference = Math.floor(difference / 1000);

    console.log(minutesDifference);
    if (minutesDifference > 5)
        return true;
    else return false;
}

function getUsers(userIds = [], nextPageToken, accessToken) {
    return getAccessToken(accessToken).then(accessToken => {
        const options = {
            method: 'POST',
            uri: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/downloadAccount?fields=users/localId,users/lastLoginAt,nextPageToken&access_token=' + accessToken,
            body: {
                nextPageToken: nextPageToken,
                maxResults: 1000
            },
            json: true
        };

        return rp(options).then(resp => {
            if (
                !resp.users
            ) {
                return userIds;
            }
            if (resp.nextPageToken) {
                return getUsers(userIds.concat(resp.users), resp.nextPageToken, accessToken);
            }
            return userIds.concat(resp.users);
        })
            ;
    })
        ;
}

/**
 * Returns an access token using the Google Cloud metadata server.
 */
function getAccessToken(accessToken) {
    // If we have an accessToken in cache to re-use we pass it directly.
    if (accessToken) {
        return Promise.resolve(accessToken);
    }

    const options = {
        uri: 'http://metadata.google.internal/computeMetadata/v1/instance/service-accounts/default/token',
        headers: {'Metadata-Flavor': 'Google'},
        json: true
    };

    return rp(options).then(resp => resp.access_token
    )
        ;
}

function resizeImageToSize(path, size, outputTempFilePath, outputFilePath) {
    return new Promise(function (resolve, reject) {
        easyimg.exec('convert ' + path + ' -resize ' +
            (size) + 'x' + (size) + '^  -gravity center -crop ' +
            (size) + 'x' + (size) + '+0+0 +repage ' + outputTempFilePath).then(
            function (file) {

                easyimg.exec('convert ' + outputTempFilePath + ' \\( +clone -threshold -1 -negate -fill white -draw "circle ' +
                    (size / 2) + ',' + (size / 2) + ' ' + (size / 2) + ',0" \\) -alpha off -compose copy_opacity -composite ' +
                    outputFilePath).then(
                    function (file) {
                        fs.unlink(outputTempFilePath, function (err) {
                            if (err) {
                                console.log(err);
                            }
                        });
                        console.log("VIDUJE CIRCLE");
                        resolve(outputFilePath);
                    }, function (err) {
                        console.log(err);
                        reject(null);
                    }
                );
            }, function (err) {
                console.log(err);
                reject(null);
            }
        );
    })
}


var upload = (localFile, remoteFile) => {

    var uuid = UUID();

    return bucket.upload(localFile, {
        destination: remoteFile,
        uploadType: "media",
        metadata: {
            contentType: 'image/png',
            metadata: {
                firebaseStorageDownloadTokens: uuid
            }
        }
    })
        .then((data) => {

            let file = data[0];

            return Promise.resolve("https://firebasestorage.googleapis.com/v0/b/" + bucket.name + "/o/" + encodeURIComponent(file.name) + "?alt=media&token=" + uuid);
        });
}
