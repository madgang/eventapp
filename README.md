# README #

How to use it?

1. Clone/download project
2. Run: *npm install*

Now you should be able to start project with *ionic serve*.

If ionic serve is working now you need to deploy project to database. Steps:

1. *firebase init*

 What do you want to use as your public directory? *www*

 Configure as a single-page app (rewrite all urls to /index.html)? *No*

+  Wrote www/404.html
 File www/index.html already exists. Overwrite? *No*

2. *firebase logon*
3. *firebase deploy*

If success link should be provided for deployed app.
**Remove browser cache, because update will be not visible.**